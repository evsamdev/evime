//
//  EVFLoadingProcessing.m
//  EVIME
//
//  Created by Evgeny on 15/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFLoadingProcessing.h"
#import "EVFEvime.h"
#import "EVFImage.h"
#import "SBJson.h"
#import "EVFAppDelegate.h"
#import "EVFElementView.h"
#import "UIImage+KTCategory.h"
#import "EVFProperty.h"
#import "EVFLogProcessing.h"
#import "EVFLogNote.h"

@implementation EVFLoadingProcessing

- (NSManagedObjectContext *)getContextForBGTask {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [context setParentContext:[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]];
    return context;
}

- (void)saveContextForBGTask:(NSManagedObjectContext *)bgTaskContext {
    if (bgTaskContext.hasChanges) {
        [bgTaskContext performBlockAndWait:^{
            NSError *error = nil;
            [bgTaskContext save:&error];
            //NSLog(@"error: %@",error.localizedDescription);
            [self saveDefaultContext];
        }];
        
    }
}

- (void)saveDefaultContext{
    NSManagedObjectContext *_defaultManagedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    //NSLog(@"saveDefaultContext");
    //NSLog(@"_defaultManagedObjectContext: %@", _defaultManagedObjectContext);
    if (_defaultManagedObjectContext.hasChanges) {
        //NSLog(@"_defaultManagedObjectContext.hasChanges");
        [_defaultManagedObjectContext performBlockAndWait:^{
            NSError *error = nil;
            [_defaultManagedObjectContext save:&error];
            //NSLog(@"default.error: %@",error.localizedDescription);
        }];
    }
}

- (void) beginBackgroundLoadingTask
{
    self.backgroundLoadingTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundLoadingTask];
    }];
}

- (void) endBackgroundLoadingTask
{
    [[UIApplication sharedApplication] endBackgroundTask: self.backgroundLoadingTask];
    self.backgroundLoadingTask = UIBackgroundTaskInvalid;
}


-(void)uploadCurrentChanges{
    [self beginBackgroundLoadingTask];
    //NSLog(@"uploadCurrentChanges");
   
    NSManagedObjectContext *managedObjectContext = [self getContextForBGTask];
     NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    
    NSArray *fullEvimeList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for(EVFEvime *evime in fullEvimeList){
        
        if([evime.readyness  isEqual: @4]){
            
            [managedObjectContext deleteObject:evime];
            
        }
    
    }
    
    [self saveContextForBGTask:managedObjectContext];
    //Profile
   
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFProperty"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithFormat:@"status=='notuploaded'"];
    
    [fetchRequest setPredicate:predicate];
    
    NSArray *propertiesList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for(EVFProperty *property in propertiesList){
        [self sendProfileData:property forContext:managedObjectContext];
    }
    
    //Evimes
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    
    predicate = [NSPredicate predicateWithFormat:@"status=='notuploaded'"];
    
    [fetchRequest setPredicate:predicate];
    
    NSArray *evimeList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for(EVFEvime *evime in evimeList){
        [self sendCurrentEvime:evime forContext:managedObjectContext];
    }
    
    //Images
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    
    predicate = [NSPredicate predicateWithFormat:@"status=='notuploaded'"];
    
    [fetchRequest setPredicate:predicate];
    
    NSArray *imageList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for(EVFImage *image in imageList){
        [self uploadImage:image forContext:managedObjectContext];
    }
    
}


-(void)uploadImages{
    
    NSManagedObjectContext *managedObjectContext = [self getContextForBGTask];
    //Images
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status=='notuploaded'"];
    
    [fetchRequest setPredicate:predicate];
    
    NSArray *imageList =[managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    for(EVFImage *image in imageList){
        [self uploadImage:image forContext:managedObjectContext];
    }

}


-(void)uploadImage:(EVFImage *)imageEnt forContext:(NSManagedObjectContext *)managedObjectContext{
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *myDocsPath    = [myPathList  objectAtIndex:0];
    NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:imageEnt.evimeUUID];
    NSString *userPhotoPath = [currentImagesDir stringByAppendingPathComponent:imageEnt.imageFile];
    
    //NSLog(@"evimeUUID: %@, userPhotoPath: %@",imageEnt.evimeUUID,userPhotoPath);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:userPhotoPath];
    UIImage *image;
    if(fileExists){
        image = [UIImage imageWithContentsOfFile:userPhotoPath];
    }else{
        return;
    }
    
    NSString *serverPhotoFile = [NSString stringWithFormat:@"%@", imageEnt.imageFile];
    
    //NSLog(@"serverPhotoFile: %@",serverPhotoFile);
    
    NSData *imageData = UIImageJPEGRepresentation(image,0.8);
    NSString *urlString = @"https://enotekit.com/mobile_app/api/upload_image.php";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ImageKey"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setTimeoutInterval:60];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@\"\r\n",serverPhotoFile] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    //NSLog(@"request: %@",request.allHTTPHeaderFields);
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.uploadimage";
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"uploadImage.request error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"upload Image responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   
                                   //NSLog(@"responseDict: %@", responseDict);
                                       
                                   if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                           
                                           //NSLog(@"image: %@, imageFile: %@", image, imageEnt.imageFile);
                                           imageEnt.status = @"uploaded";
                                           [self saveContextForBGTask:managedObjectContext];
                                           
                                           NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
                                           fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
                                           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid = %@",imageEnt.evimeUUID];
                                           [fetchRequest setPredicate:predicate];
                                           NSError *error;
                                           EVFEvime *evime =[managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                           
                                           if([evime.box isEqualToString:@"sent"]){
                                           
                                               NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
                                               fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
                                               NSPredicate *predicate = [NSPredicate predicateWithFormat:@"evimeUUID = %@",evime.uuid];
                                               [fetchRequest setPredicate:predicate];
                                               NSError *error;
                                               NSArray *imageList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
                                               //NSLog(@"imageList: %@",imageList);
                                               BOOL isAllUploaded=YES;
                                               for(EVFImage *image in imageList){
                                                   
                                                   if([image.status isEqualToString:@"notuploaded"]){
                                                       isAllUploaded=NO;
                                                   }
                                                   
                                               }
                                               
                                               if(isAllUploaded){
                                                   
                                                   NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
                                                   fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
                                                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sourceUUID = %@",evime.uuid];
                                                   [fetchRequest setPredicate:predicate];
                                                   NSError *error;
                                                   EVFEvime *sentEvime =[managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                                   if(sentEvime){
                                                       sentEvime.status = @"notuploaded";
                                                       [self saveContextForBGTask:managedObjectContext];
                                                       [self sendCurrentEvime:sentEvime forContext:managedObjectContext];
                                                   }
                                                   //NSLog(@"Evime images ready: %@",evime.title);
                                                   
                                                   
                                               }
                                           
                                           
                                           }
                                           
                                   }else{
                                   
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 2;
                                       logNote.note = [NSString stringWithFormat:@"UploadImage: %@",responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                   
                                   }
                                   
                               }
                           }];
    
    
}


-(void)sendCurrentEvime:(EVFEvime *)evime forContext:(NSManagedObjectContext *)managedObjectContext{
    
    //NSLog(@"sendCurrentEvime");
    
    NSString *deviceUUID = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    NSString *sessionKey = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/upload_evime.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    NSString *bodyEncodedString = [[evime.body dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *createDateString = [dateFormatter stringFromDate:evime.createDate];
    NSString *deleteDateString = [dateFormatter stringFromDate:evime.deleteDate];
    NSString *lasteditDateString = [dateFormatter stringFromDate:evime.lastEditDate];
    NSString *receiveDateString = [dateFormatter stringFromDate:evime.receiveDate];
    NSString *sendDateString = [dateFormatter stringFromDate:evime.sendDate];
    
    if(!createDateString){
        createDateString = @"";
    }
    
    if(!deleteDateString){
        deleteDateString = @"";
    }
    
    if(!lasteditDateString){
        lasteditDateString = @"";
    }
    
    if(!receiveDateString){
        receiveDateString = @"";
    }
    
    if(!sendDateString){
        sendDateString = @"";
    }
    
    if(evime.userFrom)[requestData setObject:evime.userFrom forKey:@"FromUsername"];
    if(evime.userTo)[requestData setObject:evime.userTo forKey:@"ToUsername"];
    [requestData setObject:evime.user forKey:@"OwnerUsername"];
    [requestData setObject:evime.uuid forKey:@"UUID"];
    
    [requestData setObject:createDateString forKey:@"CreateDate"];
    [requestData setObject:deleteDateString forKey:@"DeleteDate"];
    [requestData setObject:lasteditDateString forKey:@"LasteditDate"];
    [requestData setObject:receiveDateString forKey:@"ReceiveDate"];
    [requestData setObject:sendDateString forKey:@"SendDate"];      
    
    [requestData setObject:evime.box forKey:@"Box"];
    [requestData setObject:bodyEncodedString forKey:@"Body"];
    [requestData setObject:evime.title forKey:@"Title"];
    [requestData setObject:evime.trash forKey:@"Trash"];
    [requestData setObject:evime.group forKey:@"Group"];
    
    NSString *dataJsonString =[requestData JSONRepresentation];
    
    NSString *dataEncodedString = [[dataJsonString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
    NSString* str = [NSString stringWithFormat:@"sessionKey=%@&deviceUUID=%@&username=%@&data=%@",sessionKey,deviceUUID,username,dataEncodedString];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.uploadevime";
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                  
                                   ///NSLog(@"sendCurrentEvime.request error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"SendCurrentEvime. responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"responseDict: %@", responseDict);
                                   
                                   if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                       evime.status = @"uploaded";
                                       if([evime.box isEqualToString:@"inbox"] && evime.sourceUUID){
                                           
                                           NSError *error;
                                           NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
                                           fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
                                           NSPredicate *predicate;
                                           predicate = [NSPredicate predicateWithFormat:@"uuid==%@",evime.sourceUUID];
                                           [fetchRequest setPredicate:predicate];
                                           EVFEvime *sourceEvime =[managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                           sourceEvime.readyness = [NSNumber numberWithInteger:1];
                                           //NSLog(@"Delete sent EVIME");
                                           [managedObjectContext deleteObject:evime];
                                           [self saveContextForBGTask:managedObjectContext];
                                           [self postNotificationBackground];
                                       }else{
                                           [self saveContextForBGTask:managedObjectContext];
                                       }
                                       
                                       if([evime.trash  isEqual: @2]){
                                           
                                           evime.readyness=@4;
                                           [self saveContextForBGTask:managedObjectContext];
                                       }
                                       
                                                                               
                                   }else if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:-4]]){
                                       
                                       if([evime.box isEqualToString:@"inbox"]){
                                       
                                           evime.readyness=@4;
                                           [self saveContextForBGTask:managedObjectContext];
                                       
                                       }
                                                                              
                                   }else{
                                       
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 3;
                                       logNote.note = [NSString stringWithFormat:@"SendCurrentEvime: %@______Response %@",str,responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       
                                   }

                                   
                               }
                           }];


}


-(void)sendProfileData:(EVFProperty *)property forContext:(NSManagedObjectContext *)managedObjectContext{
    
    //NSLog(@"sendProfile");
    
    NSString *deviceUUID = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    NSString *sessionKey = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/upload_profile.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    if(property.value && property.value){
        [requestData setObject:property.key forKey:@"property"];
        [requestData setObject:property.value forKey:@"value"];
        [requestData setObject:username forKey:@"OwnerUsername"];
    }
    
    NSString *dataJsonString =[requestData JSONRepresentation];
    
    NSString *dataEncodedString = [[dataJsonString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString* str = [NSString stringWithFormat:@"sessionKey=%@&deviceUUID=%@&username=%@&data=%@",sessionKey,deviceUUID,username,dataEncodedString];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
   // NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.uploadprofile";
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"sendProfileData error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"sendProfileData. responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"responseDict: %@", responseDict);
                                   
                                   if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                       property.status = @"uploaded";
                                       [self saveContextForBGTask:managedObjectContext];
                                   }else{
                                       
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 4;
                                       logNote.note = [NSString stringWithFormat:@"SendProfileData: %@_____Response:%@",str,responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       
                                   }
                                   
                               }
                           }];
    
    
}



-(void)downloadCurrentEvimes{
    
    //NSLog(@"downloadCurrentEvimes");
    [self beginBackgroundLoadingTask];
    [self downloadImages];
    
    NSString *deviceUUID = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    NSString *sessionKey = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/download_evimes.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    
    NSString* str = [NSString stringWithFormat:@"sessionKey=%@&deviceUUID=%@&username=%@",sessionKey,deviceUUID,username];

    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.downloadevimeslist";
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"downloadCurrentEvimes.request error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSArray *responseList = [parser objectWithString:responseString];
                                   //NSLog(@"Evimes List: %@", responseList);
                                   
                                   if([responseList isKindOfClass:[NSArray class]]){
                                       
                                       for(NSDictionary *evimeData in responseList){
                                           
                                           NSString *evimeUUID = [evimeData objectForKey:@"uuid"];
                                           NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                           dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                                           NSDate *lastDate = [dateFormatter dateFromString: [evimeData objectForKey:@"lastedit_date"]];
                                           
                                           NSError *error;
                                           NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
                                           fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO],nil];
                                           NSPredicate *predicate;
                                           predicate = [NSPredicate predicateWithFormat:@"uuid==%@",evimeUUID];
                                           [fetchRequest setPredicate:predicate];
                                           NSManagedObjectContext *managedObjectContext = [self getContextForBGTask];
                                           EVFEvime *currentEvime =[managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                           
                                           //NSLog(@"currentEvime: %@, evimeData: %@ ",currentEvime.lastEditDate,[evimeData objectForKey:@"lastedit_date"]);
                                           
                                           if(!currentEvime){
                                               [self downloadEvime:evimeUUID];
                                               
                                           }else{
                                               if([lastDate compare:currentEvime.lastEditDate]>0){
                                                   [self downloadEvime:evimeUUID];
                                               
                                               }
                                           }
                                           
                                       }
                                   }else{
                                       
                                       if([responseList isKindOfClass:[NSDictionary class]]){
                                           
                                           if([[(NSDictionary *)responseList objectForKey:@"status"] isEqualToNumber:@-3]){
                                           
                                               [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsAuthenticated:NO];
                                               return;
                                           
                                           }
                                           
                                       
                                       
                                       }
                                       
                                       
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 5;
                                       logNote.note = [NSString stringWithFormat:@"DownloadCurrentEvimes Response %@",responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       
                                   }
                                   
                               }
                           }];
    
    
    
    
    
}

-(void)downloadImages{
    
    NSManagedObjectContext *managedObjectContext = [self getContextForBGTask];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status=='notdownloaded'"];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *imageList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //NSLog(@"imageList: %@",imageList);
    
    for(EVFImage *image in imageList){
        [self downloadImage:image managedObjectContext:managedObjectContext];
    }
    
}

-(void)downloadImagesForEvime:(NSString *)evimeUUID{

    NSManagedObjectContext *managedObjectContext = [self getContextForBGTask];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status=='notdownloaded' && evimeUUID=%@",evimeUUID];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *imageList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //NSLog(@"imageList: %@",imageList);
    
    for(EVFImage *image in imageList){
        [self downloadImage:image managedObjectContext:managedObjectContext];
    }

}


-(void)downloadEvime:(NSString *)uuid{
    
    //NSLog(@"downloadEvime: %@", uuid);
    
    NSString *deviceUUID = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    NSString *sessionKey = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/download_evime.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    NSString* str = [NSString stringWithFormat:@"sessionKey=%@&deviceUUID=%@&username=%@&evimeUUID=%@",sessionKey,deviceUUID,username,uuid];
    
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.downloadevime";

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"downloadEvime.request error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"responseString: %@", responseString);
                                   
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *evimeData  = [parser objectWithString:responseString];
                                   //NSLog(@"Evime Data: %@", evimeData);
                                   NSString *evimeUUID = [evimeData objectForKey:@"uuid"];
                                   
                                   if(evimeUUID){
                                       
                                       NSError *error;
                                       NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
                                       fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO],nil];
                                       NSPredicate *predicate;
                                       
                                       predicate = [NSPredicate predicateWithFormat:@"uuid==%@",evimeUUID];
                                       
                                       [fetchRequest setPredicate:predicate];
                                       NSManagedObjectContext *managedObjectContext= [self getContextForBGTask];
                                       EVFEvime *currentEvime =[managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                       
                                       EVFEvime *evime;
                                      
                                       if(currentEvime){
                                           evime = currentEvime;
                                       }else{
                                           evime = [NSEntityDescription insertNewObjectForEntityForName:@"EVFEvime" inManagedObjectContext:managedObjectContext];
                                           evime.uuid = evimeUUID;
                                       }
                                       
                                       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                       dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                                       evime.user = username;
                                       evime.title = [evimeData objectForKey:@"title"];
                                       evime.box = [evimeData objectForKey:@"box"];
                                       evime.group = [evimeData objectForKey:@"group"];
                                       evime.userTo = [evimeData objectForKey:@"to_username"];
                                       evime.userFullTo = [evimeData objectForKey:@"to_username_full"];
                                       evime.userFrom =[evimeData objectForKey:@"from_username"];
                                       evime.userFullFrom =[evimeData objectForKey:@"from_username_full"];
                                       evime.createDate = [dateFormatter dateFromString:[evimeData objectForKey:@"create_date"]];
                                       evime.deleteDate = [dateFormatter dateFromString:[evimeData objectForKey:@"delete_date"]];
                                       evime.lastEditDate =[dateFormatter dateFromString: [evimeData objectForKey:@"lastedit_date"]];
                                       evime.receiveDate = [dateFormatter dateFromString:[evimeData objectForKey:@"receive_date"]];
                                       evime.sendDate = [dateFormatter dateFromString:[evimeData objectForKey:@"send_date"]];
                                       evime.trash = [NSNumber numberWithInteger:[[evimeData objectForKey:@"trash"] integerValue]];
                                       evime.status = @"downloaded";
                                       evime.readyness = [NSNumber numberWithInteger:0];
                                       
                                       NSData *bodyData = [[NSData alloc] initWithBase64EncodedString:[evimeData objectForKey:@"body"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                                       
                                       NSString *bodyDecodedString = [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding];
                                       evime.body = bodyDecodedString;
                                       
                                       SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
                                       
                                       NSDictionary *evimeBodyData = [jsonParser objectWithString:bodyDecodedString];
                                       
                                       NSDictionary *mainPageData = [evimeBodyData objectForKey:@"page"];
                                       NSArray *elmentsDataList =  [mainPageData objectForKey:@"elements"];
                                       //NSLog(@"elmentsDataList: %@",elmentsDataList);
                                       BOOL isAnyImages=[self processElementsDataList:elmentsDataList forEvimeUUID:evime.uuid managedObjectContext:managedObjectContext];
                                       if(!isAnyImages){
                                           evime.readyness = [NSNumber numberWithInteger:1];
                                       }
                                       [self saveContextForBGTask:managedObjectContext];
                                       [self downloadImagesForEvime:evime.uuid];
                                       
                                       [self postNotificationBackground];
                                       
                                   }else{
                                       
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 6;
                                       logNote.note = [NSString stringWithFormat:@"DownloadEvime Response %@",responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       
                                   }
                                   
                               }
                           }];
    
    
    
    
    
    
}




-(BOOL)processElementsDataList:(NSArray *)elmentsDataList forEvimeUUID:(NSString *)evimeUUID managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    BOOL isAnyImages=NO;
    
    for(NSDictionary *elementData in elmentsDataList){
    
        NSInteger type = [[elementData objectForKey:@"type"] integerValue];
        
        if(type==kElementTypeImage){
            
            NSString *imageFileName = [elementData objectForKey:@"content"];
            if(imageFileName){
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imageFile==%@ && evimeUUID==%@",imageFileName,evimeUUID];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
                [fetchRequest setPredicate:predicate];
               
                EVFImage *image =[managedObjectContext executeFetchRequest:fetchRequest error:nil].lastObject;
                if(!image){
                    image = [NSEntityDescription insertNewObjectForEntityForName:@"EVFImage" inManagedObjectContext:managedObjectContext];
                    image.evimeUUID = evimeUUID;
                    image.imageFile = imageFileName;
                    image.status = @"notdownloaded";
                    isAnyImages = YES;
                }else{
                    //NSLog(@"image Already Exist: %@",imageFileName);
                }
            }else{
                //NSLog(@"imageFileName Not Exist");
            }
        }
        
        NSDictionary *childPageData = [elementData objectForKey:@"page"];
        NSArray *elmentsDataList =  [childPageData objectForKey:@"elements"];
        
        BOOL _isAnyImages = [self processElementsDataList:elmentsDataList forEvimeUUID:evimeUUID managedObjectContext:managedObjectContext];
        if(_isAnyImages){
            isAnyImages = YES;
        }
        
    }
    
    return isAnyImages;
}



-(void)downloadImage:(EVFImage *)image managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    //NSLog(@"downloadImage for evime: %@",image.evimeUUID);
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.downloadimage";
    
    NSString *dir = [image.imageFile substringToIndex:3];
    NSString *file = [image.imageFile substringFromIndex:3];
    
    NSString *urlString = [NSString stringWithFormat:@"https://enotekit.com/mobile_app/api/uploads/%@/%@",dir,file];
    //NSLog(@"urlString: %@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"downloadImage: request error: %@", error.localizedDescription);
                                   
                               }else{
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   
                                   UIImage *newImage = [UIImage imageWithData:data];
                                   if(!newImage){
                                       
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 7;
                                       logNote.note = [NSString stringWithFormat:@"DownloadImage %@_____Response %@",urlString,responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       return;
                                   }
                                   
                                   UIImage *thumbImage;
                                   UIImage *fullImage;
                                   fullImage = [newImage imageScaleAspectToMaxSize:1280];
                                   
                                   if(newImage.size.height>newImage.size.width){
                                       
                                       thumbImage = [newImage imageScaleAspectToMaxSize:560];
                                       
                                   }else{
                                       
                                       thumbImage = [newImage imageScaleAspectToMaxSize:440];
                                       
                                   }
                                  
                                   NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                   NSString *myDocsPath    = [myPathList  objectAtIndex:0];
                                   NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:image.evimeUUID];
                                   NSError *error;
                                   BOOL isDir;
                                   //NSLog(@"currentImagesDir: %@",currentImagesDir);
                                   if(![[NSFileManager defaultManager] fileExistsAtPath:currentImagesDir isDirectory:&isDir])
                                   {
                                       //NSLog(@"currentImagesDir Not Exist");
                                       [[NSFileManager defaultManager] createDirectoryAtPath:currentImagesDir withIntermediateDirectories:YES attributes:nil error:&error];
                                       
                                   }
                                   
                                   NSString *thumbImageFileName = [NSString stringWithFormat:@"th_%@",image.imageFile];
                                  
                                   NSString *jpgPath = [currentImagesDir stringByAppendingPathComponent:thumbImageFileName];
                                   [UIImageJPEGRepresentation(thumbImage, 0.8) writeToFile:jpgPath atomically:YES];
                                   
                                   NSString *fullImageFileName = [NSString stringWithFormat:@"%@",image.imageFile];
                                   
                                   jpgPath = [currentImagesDir stringByAppendingPathComponent:fullImageFileName];
                                   [UIImageJPEGRepresentation(fullImage, 0.8) writeToFile:jpgPath atomically:YES];
                                   
                                   //NSLog(@"DownloadImage. evimeUUID: %@, imageFile: %@",image.evimeUUID, image.imageFile);
                                   //NSLog(@"image: %@",image);
                                   image.status = @"downloaded";
                                   [self saveContextForBGTask:managedObjectContext];
                                   
                                   NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
                                   fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
                                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"evimeUUID = %@",image.evimeUUID];
                                   [fetchRequest setPredicate:predicate];
                                   NSArray *imageList =[managedObjectContext executeFetchRequest:fetchRequest error:&error];
                                   
                                   BOOL isAllUploaded=YES;
                                   for(EVFImage *image in imageList){
                                       
                                       if([image.status isEqualToString:@"notdownloaded"]){
                                           isAllUploaded=NO;
                                       }
                                       
                                   }
                                   
                                   if(isAllUploaded){
                                      
                                       NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
                                       fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
                                       NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid = %@",image.evimeUUID];
                                       [fetchRequest setPredicate:predicate];
                                       NSError *error;
                                       EVFEvime *evime =[managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                       evime.readyness = [NSNumber numberWithInteger:1];
                                       [self saveContextForBGTask:managedObjectContext];
                                       
                                       //NSLog(@"Evime images ready: %@",evime.title);
                                       [self postNotificationBackground];
                                       
                                   }
                                   
                                   
                                   
                                   
                                   
                               }
                           }];






}

-(void)updateToken{
    
    //NSLog(@"updateToken");
    NSString *deviceToken = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceToken];
    NSString *deviceUUID =  [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];

    NSString *sessionKey = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/update_token.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    NSString* str = [NSString stringWithFormat:@"sessionKey=%@&deviceUUID=%@&username=%@&deviceToken=%@",sessionKey,deviceUUID,username,deviceToken];
    
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.updatetoken";
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"updateToken error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];

                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"UpdateToken.responseDict: %@", responseDict);
                                   
                                   if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                   
                                   }
                               }
                           }];



}

-(void)logout{
    
    //NSLog(@"logout");
    
    NSString *deviceUUID =  [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/logout.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    NSString* str = [NSString stringWithFormat:@"deviceUUID=%@&username=%@",deviceUUID,username];
    
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.evforest.logout";
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                  // NSLog(@"updateToken error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"Logout.responseDict: %@", responseDict);
                                   
                                   if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                       
                                   }
                               }
                           }];
    
    
    
}



//Call this to post a notification and are on a background thread
- (void) postNotificationBackground{
    [self performSelectorOnMainThread:@selector(postNotificationMain) withObject:nil waitUntilDone:NO];
}

//Do not call this directly if you are running on a background thread.
- (void) postNotificationMain{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.evforest.StateOfEvimesListChanged" object:self];
}

@end
