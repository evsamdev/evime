    //
//  EVFEvimesListViewController.m
//  EVIME
//
//  Created by Evgeny on 27/10/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFEvimesListViewController.h"
#import "EVFEvime.h"
#import "EVFConstructViewController.h"
#import "EVFAppDelegate.h"
#import "EVFReplayViewController.h"
#import "SBJson.h"
#import "EVFElementView.h"
#import "EVFImage.h"
#import "EVFContact.h"
#import "EVFLoadingProcessing.h"
#import "EVFEvimesListTableViewCell.h"
#import "EVFLogNote.h"
#import "EVFLogProcessing.h"

@interface EVFEvimesListViewController ()<UITextFieldDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *evimesList;
@property (strong, nonatomic) NSArray *contactsList;
@property (nonatomic) BOOL isEditingMode;
@property (strong, nonatomic)  UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger selectedIndex;
@end

@implementation EVFEvimesListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    //NSLog(@"self.box: %@", self.box);
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES],nil];
    self.contactsList=[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
    self.refreshControl = [[UIRefreshControl alloc]
                           init];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.navigationController.navigationItem.backBarButtonItem.tintColor = [UIColor whiteColor];
}

-(void)reloadTableDataNotification{

    //NSLog(@"reloadTableDataNotification");
    [self reloadTableData];
}

-(void)updateEvimesList{

    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
    
    NSPredicate *predicate;
    NSString *username;
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    }else{
        username = @"_#_AnonimoussuominonA_#_";
    }
    
    if([self.box isEqualToString:@"trash"]){
        
        predicate = [NSPredicate predicateWithFormat:@"trash==1 && user == %@",username];
        fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"deleteDate" ascending:NO],nil];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteAll:)] ;
        
    }else{
        
        predicate = [NSPredicate predicateWithFormat:@"box == %@ && trash==0 && user == %@",self.box,username];
        self.navigationItem.rightBarButtonItem = nil;
        
        if([self.box isEqualToString:@"sent"]){
            fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"sendDate" ascending:NO],nil];
        }else if([self.box isEqualToString:@"inbox"]){
            fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"sendDate" ascending:NO],nil];
        }else if([self.box isEqualToString:@"draft"]){
            fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"lastEditDate" ascending:NO],nil];
        }
        
    }
    [fetchRequest setPredicate:predicate];
    
    self.evimesList=[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];


}


-(void)reloadTableData{
    
    //NSLog(@"reloadTableData");
    [self updateEvimesList];
    [self.evimesTableView reloadData];

}

-(void)viewWillDisappear:(BOOL)animated{
    //NSLog(@"viewWillDisappear");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.evforest.StateOfEvimesListChanged" object:nil];
    [self.refreshControl removeFromSuperview];
  

}

-(void)viewDidDisappear:(BOOL)animated{
    [self.evimesTableView setEditing:NO];

}

-(void)updateIconBadge{
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"box == 'inbox' && trash==0 && user == %@ && readyness = 1",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
    [fetchRequest setPredicate:predicate];
    NSArray *evimesUnreadList=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(evimesUnreadList.count>0){
        [UIApplication sharedApplication].applicationIconBadgeNumber = evimesUnreadList.count;
    }
    
    
}

-(void)refresh{
    
    if([self.box isEqualToString:@"sent"]){
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    }
    
    if([self.box isEqualToString:@"inbox"] || [self.box isEqualToString:@"draft"]){
        [[[EVFLoadingProcessing alloc] init] downloadCurrentEvimes];
    }
    
    
    
    //NSLog(@"refresh");
    
    [self performSelector:@selector(endRefreshing) withObject:nil afterDelay:2.0];

}

-(void)endRefreshing{

    [self.refreshControl endRefreshing];

}

-(void)viewWillAppear:(BOOL)animated{
    /*
    NSArray *array =[[NSArray alloc] init];
    [array objectAtIndex:0];
    */
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableDataNotification) name:@"com.evforest.StateOfEvimesListChanged" object:nil];
    
    if(self.isPushToCreateNewEvime){
    
        [self createNewEvime:nil];
        self.isPushToCreateNewEvime = NO;
    }
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] destinationBox]){
    
        self.box = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] destinationBox];
        
        if([self.box isEqualToString:@"draft"]){
        
            self.title = @"Drafts";
        }
        
        if([self.box isEqualToString:@"sent"]){
            
            self.title = @"Sent";
        }
        
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setDestinationBox:nil];
        
    }
    
    
    if(![self.box isEqualToString:@"trash"]){
    
        [self.evimesTableView addSubview:self.refreshControl];
    
    }
    

    [self reloadTableData];
    [self editCurrentEvime];

}


-(void)editCurrentEvime{
    
    EVFEvime *currentEvimeForEdit=[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] evimeForEdit];
    if(currentEvimeForEdit){
        
        SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
        
        NSDictionary *evimeBodyData = [jsonParser objectWithString:currentEvimeForEdit.body];
        
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        if([currentEvimeForEdit.box isEqualToString:@"draft"]){
            
            EVFConstructViewController * constructViewController = (EVFConstructViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFConstruct"];
            constructViewController.title = currentEvimeForEdit.title;
            constructViewController.pageData = [evimeBodyData objectForKey:@"page"];
            constructViewController.bodyData = [evimeBodyData mutableCopy];
            constructViewController.currentEvime = currentEvimeForEdit;
            constructViewController.pageNumber = 1;
            
            [self.navigationController pushViewController:constructViewController animated:NO];
        
        }else{
        
            EVFEvime *newEvime = [NSEntityDescription insertNewObjectForEntityForName:@"EVFEvime" inManagedObjectContext:self.managedObjectContext];
            
            EVFConstructViewController * constructViewController = (EVFConstructViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFConstruct"];
            
            constructViewController.title = currentEvimeForEdit.title;
            constructViewController.pageData = [evimeBodyData objectForKey:@"page"];;
            constructViewController.bodyData = [evimeBodyData mutableCopy];
            constructViewController.currentEvime = newEvime;
            constructViewController.pageNumber = 1;
            
            NSString *UUID = [[NSUUID UUID] UUIDString];
            newEvime.user = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
            newEvime.box = @"draft";
            newEvime.body = currentEvimeForEdit.body;
            newEvime.uuid = UUID;
            newEvime.createDate = [NSDate date];
            newEvime.index = nil;//[NSNumber numberWithInt:0];
            newEvime.trash = [NSNumber numberWithInt:0];
            newEvime.status = @"notuploaded";
            newEvime.title = currentEvimeForEdit.title;
            newEvime.group = @"main";
            newEvime.lastEditDate = [NSDate date];
            newEvime.readyness = [NSNumber numberWithInteger:2];
            
            NSError *error;
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
            fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
            NSPredicate *predicate;
            predicate = [NSPredicate predicateWithFormat:@"evimeUUID=%@", currentEvimeForEdit.uuid];
            [fetchRequest setPredicate:predicate];
            NSArray *imageList =[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            for(EVFImage *curImage in imageList){
            
                EVFImage *image = [NSEntityDescription insertNewObjectForEntityForName:@"EVFImage" inManagedObjectContext:self.managedObjectContext];
                image.evimeUUID = newEvime.uuid;
                image.imageFile = curImage.imageFile;
                image.status = curImage.status;

            }
                        
            [self.managedObjectContext save:&error];
            [self.navigationController pushViewController:constructViewController animated:NO];
            
            NSString *evimeUUID = currentEvimeForEdit.uuid;
            
            NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *myDocsPath    = [myPathList  objectAtIndex:0];
            NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:evimeUUID];
            NSString *newImagesDir = [myDocsPath stringByAppendingPathComponent:newEvime.uuid];
            
            [[NSFileManager defaultManager] copyItemAtPath:currentImagesDir toPath:newImagesDir error:&error];
            
            
        }
        
        
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"self.evimesList.count: %lu,", (unsigned long)self.evimesList.count);
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.evimesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EVFEvime *evime = [self.evimesList objectAtIndex:indexPath.row];
    
    NSString *evimeTitle = evime.title;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //dateFormatter.dateFormat =   @"dd MMM yyyy HH:mm";
    
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    static NSString *CellIdentifier = @"EvimeCell";
    EVFEvimesListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.titleLabel.text = evimeTitle;
    
    NSString *detailString;
    if([self.box isEqualToString:@"trash"]){
        
        cell.myAccessoryButton.tag = indexPath.row + 10000;
        [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"undo.png"] forState:UIControlStateNormal];
        [cell.myAccessoryButton addTarget:self action:@selector(backEvime:) forControlEvents:UIControlEventTouchUpInside];
        detailString = [NSString stringWithFormat:@"%@, deleted: %@",[evime.box capitalizedString],[dateFormatter stringFromDate:evime.deleteDate]];
        
    }
    
    if([evime.box isEqualToString:@"draft"]){
        
        cell.nameLabel.text=@"";
        
        if(![self.box isEqualToString:@"trash"]){
            detailString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:evime.lastEditDate]];
            
            cell.myAccessoryButton.tag = indexPath.row + 30000;
            [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
            [cell.myAccessoryButton addTarget:self action:@selector(evimeInfo:) forControlEvents:UIControlEventTouchUpInside];
        }
    
    }else if([evime.box isEqualToString:@"inbox"]){
        if(evime.userFullFrom && ![evime.userFullFrom isEqualToString:@""]){
            cell.nameLabel.text = [NSString stringWithFormat:@"%@ (%@)",evime.userFrom,evime.userFullFrom];
        }else{
            cell.nameLabel.text = [NSString stringWithFormat:@"%@",evime.userFrom];
        }
        if(![self.box isEqualToString:@"trash"]){
            detailString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:evime.sendDate]];
            EVFContact *contact = [self.contactsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"username=%@ && user=%@",evime.userFrom,[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]]].lastObject;
            if(!contact){
                
                cell.myAccessoryButton.tag = indexPath.row + 20000;
                [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"add-user.png"] forState:UIControlStateNormal];
                [cell.myAccessoryButton removeTarget:self action:@selector(evimeInfo:) forControlEvents:UIControlEventTouchUpInside];
                [cell.myAccessoryButton addTarget:self action:@selector(addUserToContacts:) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
                
                cell.myAccessoryButton.tag = indexPath.row + 30000;
                [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
                [cell.myAccessoryButton removeTarget:self action:@selector(addUserToContacts:) forControlEvents:UIControlEventTouchUpInside];
                [cell.myAccessoryButton addTarget:self action:@selector(evimeInfo:) forControlEvents:UIControlEventTouchUpInside];
                
            }
        }
        
    }else if([evime.box isEqualToString:@"sent"]){
        
        if(evime.userFullTo && ![evime.userFullTo isEqualToString:@""]){
            cell.nameLabel.text = [NSString stringWithFormat:@"%@ (%@)",evime.userTo,evime.userFullTo];
        }else{
            cell.nameLabel.text = [NSString stringWithFormat:@"%@",evime.userTo];
        }
        if(![self.box isEqualToString:@"trash"]){
            
            detailString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:evime.sendDate]];
            EVFContact *contact = [self.contactsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"username=%@ && user = %@",evime.userTo, [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]]].lastObject;
            if(!contact){
                
                cell.myAccessoryButton.tag = indexPath.row + 20000;
                [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"add-user.png"] forState:UIControlStateNormal];
                [cell.myAccessoryButton removeTarget:self action:@selector(evimeInfo:) forControlEvents:UIControlEventTouchUpInside];
                [cell.myAccessoryButton addTarget:self action:@selector(addUserToContacts:) forControlEvents:UIControlEventTouchUpInside];
                 
                
            }else{
                
                cell.myAccessoryButton.tag = indexPath.row + 30000;
                [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
                [cell.myAccessoryButton removeTarget:self action:@selector(addUserToContacts:) forControlEvents:UIControlEventTouchUpInside];
                [cell.myAccessoryButton addTarget:self action:@selector(evimeInfo:) forControlEvents:UIControlEventTouchUpInside];
                
            }
        }
        
        
    }
    
    //NSLog(@"evime.readyness: %@",evime.readyness);
    
    if([evime.readyness isEqualToNumber:[NSNumber numberWithInteger:0]]){
        
        cell.titleLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
        cell.nameLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
        cell.dateLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
        [cell.myAccessoryButton setBackgroundImage:[UIImage imageNamed:@"progress.png"] forState:UIControlStateNormal];
        
    }else{
        
        if([self.box isEqualToString:@"inbox"] && [evime.readyness isEqualToNumber:[NSNumber numberWithInteger:1]]){
            cell.titleLabel.textColor = [UIColor colorWithRed:0.1 green:0.5 blue:0.8 alpha:1.0];
            cell.nameLabel.textColor = [UIColor colorWithRed:0.1 green:0.5 blue:0.8 alpha:1.0];
            cell.dateLabel.textColor = [UIColor colorWithRed:0.1 green:0.5 blue:0.8 alpha:1.0];
            
            
            
        }else{
            cell.titleLabel.textColor = [UIColor blackColor];
            cell.nameLabel.textColor = [UIColor colorWithWhite:0.2 alpha:1.0];
            cell.dateLabel.textColor = [UIColor darkGrayColor];
        }
        
        
    }
    
    cell.dateLabel.text = detailString;
    
    
    if(self.isEditingMode){
        cell.myAccessoryButton.hidden = YES;
        
    }else{
        cell.myAccessoryButton.hidden = NO;
    
    }
    
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"rowIndex: %lu",(unsigned long)indexPath.row);
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        EVFEvime *evime = [self.evimesList objectAtIndex:indexPath.row];
        if([self.box isEqualToString:@"trash"]){
                        
            NSString *evimeUUID = evime.uuid;
            NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *myDocsPath    = [myPathList  objectAtIndex:0];
            NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:evimeUUID];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:currentImagesDir error:&error];
                        
            evime.trash=[NSNumber numberWithInteger:2];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
            fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
            NSPredicate *predicate;
            predicate = [NSPredicate predicateWithFormat:@"evimeUUID==%@",evimeUUID];
            [fetchRequest setPredicate:predicate];
            NSArray *imagesList =[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            for(EVFImage *image in imagesList){
                [self.managedObjectContext deleteObject:image];
            }
            
        }else{
        
            evime.trash = [NSNumber numberWithInt:1];
        }
        
        evime.status = @"notuploaded";
        evime.deleteDate = [NSDate date];
        evime.lastEditDate = [NSDate date];
        NSError *error;
        [self.managedObjectContext save:&error];

        [self updateEvimesList];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView endUpdates];
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.4];
        self.evimesTableView.userInteractionEnabled = NO;
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
            [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
        }
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EVFEvime *evime = [self.evimesList objectAtIndex:indexPath.row];
    
    if([evime.readyness isEqualToNumber:[NSNumber numberWithInteger:0]] && [evime.box isEqualToString:@"inbox"]){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    NSString *evimeBody = evime.body;
    
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    
    NSDictionary *evimeBodyData = [jsonParser objectWithString:evimeBody];
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setDestinationBox:nil];
    EVFReplayViewController * replayViewController = (EVFReplayViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFReplay"];
    replayViewController.currentEvime = evime;
    replayViewController.pageData = [evimeBodyData objectForKey:@"page"];
    replayViewController.title = evime.title;
    replayViewController.isFirstPage = YES;
    [self.navigationController pushViewController:replayViewController animated:YES];
    
    if([evime.box isEqualToString:@"inbox"]){
        evime.readyness = @3;
        NSError *error;
        [self.managedObjectContext save:&error];
        [self updateIconBadge];
        
    }
    
 
}

-(void)evimeInfo:(UIButton *)sender{
    //NSLog(@"evimeInfo: %ld",sender.tag - 30000);
    
    self.selectedIndex = sender.tag - 30000;
    EVFEvime *evime = [self.evimesList objectAtIndex:self.selectedIndex];
    if([evime.box isEqualToString:@"draft"]){
        UIAlertView *enterTitleAlert = [[UIAlertView alloc] initWithTitle:@"Edit Title" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        enterTitleAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *title = [enterTitleAlert textFieldAtIndex:0];
        title.delegate = self;
        title.text = evime.title;
        [enterTitleAlert show];
    }else{
        NSString *userFullName;
        
        if([evime.box isEqualToString:@"sent"]){
            if([evime.userFullTo isEqualToString:@""] || !evime.userFullTo){
                userFullName = [NSString stringWithFormat:@"%@",evime.userTo];
            }else{
                userFullName = [NSString stringWithFormat:@"%@ (%@)",evime.userTo,evime.userFullTo];
            }
            
        }else  if([evime.box isEqualToString:@"inbox"]){
            if([evime.userFullFrom isEqualToString:@""] || !evime.userFullFrom){
                userFullName = [NSString stringWithFormat:@"%@",evime.userFrom];
            }else{
                userFullName = [NSString stringWithFormat:@"%@ (%@)",evime.userFrom,evime.userFullFrom];
            }
        }
        
        
        UIAlertView *enterTitleAlert = [[UIAlertView alloc] initWithTitle:evime.title message:userFullName delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [enterTitleAlert show];
    
    }


}


#pragma mark - Actions

-(void)backEvime:(UIButton *)sender{

    NSUInteger rowIndex = sender.tag-10000;
    
    //NSLog(@"rowIndex: %lu",(unsigned long)rowIndex);
    
    EVFEvime *evime = [self.evimesList objectAtIndex:rowIndex];
    evime.trash = [NSNumber numberWithInt:0];
    evime.status = @"notuploaded";
    evime.lastEditDate = [NSDate date];
    NSError *error;
    [self.managedObjectContext save:&error];
    
    [self updateEvimesList];
    
    [self.evimesTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.4];
    self.evimesTableView.userInteractionEnabled = NO;
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    }
}

-(void)reloadTable{
    
    self.evimesTableView.userInteractionEnabled = YES;
    [self.evimesTableView reloadData];

}

-(void)addUserToContacts:(UIButton *)sender{
    
    //NSLog(@"addUserToContacts: %ld",sender.tag - 20000);
    
    NSInteger index = sender.tag - 20000;
    EVFEvime *evime = [self.evimesList objectAtIndex:index];
    
    EVFContact *contact;
    contact  = [NSEntityDescription insertNewObjectForEntityForName:@"EVFContact" inManagedObjectContext:self.managedObjectContext];
    
    if([evime.box isEqualToString:@"sent"]){
        contact.username = evime.userTo;
        contact.fullName = evime.userFullTo;
        
    }else  if([evime.box isEqualToString:@"inbox"]){
        contact.username = evime.userFrom;
        contact.fullName = evime.userFullFrom;
    }
    contact.user = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    [self.managedObjectContext save:nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES],nil];
    
    self.contactsList=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    [self.evimesTableView reloadData];
    

}



- (IBAction)editMode:(UIBarButtonItem *)sender {
    
    self.isEditingMode = !self.isEditingMode;
    if(self.isEditingMode){
        [self.evimesTableView setEditing:YES animated:YES];
        [self.editButton setTitle:@"Done"];
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.4];
        
    }else{
        [self.evimesTableView reloadData];
        [self performSelector:@selector(backToEditingOff) withObject:nil afterDelay:0.1];
        
        
    }
    
}


-(void)backToEditingOff{


    [self.evimesTableView setEditing:NO animated:YES];
    [self.editButton setTitle:@"Edit"];


}


#pragma mark - Create new Evime

- (IBAction)createNewEvime:(UIBarButtonItem *)sender {
    UIAlertView *enterTitleAlert = [[UIAlertView alloc] initWithTitle:@"Title:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    enterTitleAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *title = [enterTitleAlert textFieldAtIndex:0];
    title.delegate = self;
    [enterTitleAlert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if([alertView.title isEqualToString:@"Delete All"]){
    
        if(buttonIndex==1){
        
            [self deleteAllTrashItems];
        
        }
    
    
    }else{
    
        if(buttonIndex==1){
            if([alertView.title isEqualToString:@"Edit Title"]){
                UITextField *title = [alertView textFieldAtIndex:0];
                EVFEvime *evime = [self.evimesList objectAtIndex:self.selectedIndex];
                evime.title = title.text;
                evime.status = @"notuploaded";
                evime.lastEditDate = [NSDate date];
                [self.managedObjectContext save:nil];
                if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
                    [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
                }
                [self.evimesTableView reloadData];
            }else{
                
                UITextField *title = [alertView textFieldAtIndex:0];
                if(self.isEditingMode){
                    [self editMode:nil];
                }
                [self createNewEvimeWithTitle:title.text];
            }
        }
        
    }
}

-(void)deleteAllTrashItems{

    for(EVFEvime *evime in self.evimesList){
    
        if([self.box isEqualToString:@"trash"]){
            
            NSString *evimeUUID = evime.uuid;
            NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *myDocsPath    = [myPathList  objectAtIndex:0];
            NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:evimeUUID];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:currentImagesDir error:&error];
            
            evime.trash=[NSNumber numberWithInteger:2];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
            fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
            NSPredicate *predicate;
            predicate = [NSPredicate predicateWithFormat:@"evimeUUID==%@",evimeUUID];
            [fetchRequest setPredicate:predicate];
            NSArray *imagesList =[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            for(EVFImage *image in imagesList){
                [self.managedObjectContext deleteObject:image];
            }
            
            
        }
        evime.status = @"notuploaded";
        evime.lastEditDate = [NSDate date];
        
    }
    
    NSError *error;
    [self.managedObjectContext save:&error];
    
    [self updateEvimesList];
    
    [self.evimesTableView reloadData];
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    }

}


- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    
    if([alertView.title isEqualToString:@"Delete All"]){
        return YES;
    }
    
    
    UITextField *title = [alertView textFieldAtIndex:0];
    if(title.text.length==0){
        return NO;
    }
    
    
    
    
    return YES;
}

-(void)createNewEvimeWithTitle:(NSString *)title{
    
    NSMutableDictionary *bodyData = [[NSMutableDictionary alloc] init];
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSMutableArray *elementsDataList = [[NSMutableArray alloc] init];
        
    NSMutableDictionary *pageData = [[NSMutableDictionary alloc] init];
    
    [pageData setObject:elementsDataList forKey:@"elements"];
    
    [bodyData setObject:pageData forKey:@"page"];
    
    EVFEvime *newEvime = [NSEntityDescription insertNewObjectForEntityForName:@"EVFEvime" inManagedObjectContext:self.managedObjectContext];
    
    EVFConstructViewController * constructViewController = (EVFConstructViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFConstruct"];
    constructViewController.title = title;
    constructViewController.pageData = pageData;
    constructViewController.bodyData = bodyData;
    constructViewController.currentEvime = newEvime;
    constructViewController.pageNumber = 1;
    
    NSString *UUID = [[NSUUID UUID] UUIDString];
    
    EVFLogNote *logNote = [[EVFLogNote alloc] init];
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        newEvime.user = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
        logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
        logNote.eventId = 110;
        logNote.note = UUID;
    }else{
        newEvime.user = @"_#_AnonimoussuominonA_#_";
        logNote.username = @"Anonimus";
        logNote.eventId = 111;
        logNote.note = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    }
    [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
    
    newEvime.box = @"draft";
    newEvime.body = [bodyData JSONRepresentation];
    newEvime.uuid = UUID;
    newEvime.createDate = [NSDate date];
    newEvime.index = nil;//[NSNumber numberWithInt:0];
    newEvime.trash = [NSNumber numberWithInt:0];
    newEvime.status = @"notuploaded";
    newEvime.title = title;
    newEvime.group = @"main";
    newEvime.lastEditDate = [NSDate date];
    newEvime.readyness = [NSNumber numberWithInteger:2];
        
    NSError *error;
    [self.managedObjectContext save:&error];
   
    
    
    
    [self.navigationController pushViewController:constructViewController animated:YES];
}



- (IBAction)deleteAll:(UIBarButtonItem *)sender {
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete All" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    [alert show];
    
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([string isEqualToString:@""]){
        return YES;
    }
    
    NSString *text = [textField.text stringByAppendingString:string];
    
    if(text.length>250){
        return NO;
    }
    
    
    return YES;
}



@end
