//
//  EVFViewController.h
//  EVIME
//
//  Created by Evgeny on 28.09.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
- (IBAction)createNewEvime:(UIBarButtonItem *)sender;
- (void)logout;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
