//
//  EVFCreateAccountRequest.m
//  EVIME
//
//  Created by Evgeny on 7/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFCreateAccountRequest.h"
#import "EVFAccount.h"
#import "EVFCryptoHelper.h"
#import "SBJson.h"
#import "EVFAppDelegate.h"
#import "EVFProperty.h"
#import "EVFLogProcessing.h"
#import "EVFLogNote.h"

@interface EVFCreateAccountRequest()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@implementation EVFCreateAccountRequest

-(void)sendRequestForAccount:(EVFAccount *)account {
    
    NSString *passKey = [EVFCryptoHelper passKeyForUsername:account.username andPassword:account.password ];
    
    //NSLog(@"passKey:%@",passKey);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/create_account.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    [requestData setObject:account.username forKey:@"username"];
    [requestData setObject:account.email forKey:@"email"];
    [requestData setObject:account.fullName forKey:@"fullName"];
    [requestData setObject:passKey forKey:@"passKey"];
    
    
    NSString *requestJson = [requestData JSONRepresentation];
    
    NSString *bodyEncodedString = [[requestJson dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
    NSString* str = [NSString stringWithFormat:@"data=%@",bodyEncodedString];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection failed!" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                   [alertView show];
                                   //NSLog(@"request error: %@", error.localizedDescription);
                                   [self.delegate createAccountDidFail];
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"responseDict: %@", responseDict);
                                   
                                   if(responseDict){
                                       
                                       if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                           
                                           [self.delegate createAccountDidFinish];
                                           
                                           
                                       }else if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:-4]]){
                                           
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User with that username already exists." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate createAccountDidFail];
                                           
                                       }else{
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unexpected error!" message:@"Please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate createAccountDidFail];
                                           
                                           EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                           logNote.eventId = 9;
                                           logNote.note = [NSString stringWithFormat:@"RegisterRequest:%@_____Response %@",str,responseString];
                                           logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                           
                                           [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];

                                       }
                                       
                                       
                                   }else{
                                       
                                       UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unexpected error!" message:@"Please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                       [alertView show];
                                       
                                       [self.delegate createAccountDidFail];
                                       
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 9;
                                       logNote.note = [NSString stringWithFormat:@"RegisterRequest:%@_____Response %@",str,responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       
                                   }
                                   
                                   
                               }
                               
                               
                           }];
    
    
}


@end
