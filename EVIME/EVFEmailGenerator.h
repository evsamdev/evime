//
//  EVFEmailGenerator.h
//  EVIME
//
//  Created by Evgeny on 3/05/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EVFEvime.h"

@interface EVFEmailGenerator : NSObject
-(NSString *)htmlEmailBodyForEvime:(EVFEvime *)evime;
@end
