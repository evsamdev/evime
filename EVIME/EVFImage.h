//
//  EVFImage.h
//  EVIME
//
//  Created by Evgeny on 16/03/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EVFImage : NSManagedObject

@property (nonatomic, retain) NSString * evimeUUID;
@property (nonatomic, retain) NSString * imageFile;
@property (nonatomic, retain) NSString * status;

@end
