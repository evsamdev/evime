//
//  UIImagePickerController+UIImagePickerController_NonRotating.h
//  EVIME
//
//  Created by Evgeny on 12/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImagePickerController (UIImagePickerController_NonRotating)

- (BOOL)shouldAutorotate;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;

@end
