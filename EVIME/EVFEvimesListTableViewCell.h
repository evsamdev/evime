//
//  EVFEvimesListTableViewCell.h
//  EVIME
//
//  Created by Evgeny on 12/05/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFEvimesListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIButton *myAccessoryButton;

@end
