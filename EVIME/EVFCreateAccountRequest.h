//
//  EVFCreateAccountRequest.h
//  EVIME
//
//  Created by Evgeny on 7/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EVFAccount;
@protocol EVFCreateAccountRequestDelegate <NSObject>
-(void) createAccountDidFinish;
-(void) createAccountDidFail;
@end

@interface EVFCreateAccountRequest : NSObject

@property (weak, nonatomic) id<EVFCreateAccountRequestDelegate> delegate;
-(void)sendRequestForAccount:(EVFAccount *)account;

@end










