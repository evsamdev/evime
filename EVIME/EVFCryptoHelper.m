//
//  TGACryptoHelper.m
//  CheckPoint
//
//  Created by Evgeny on 18.08.13.
//  Copyright (c) 2013 vittoria. All rights reserved.
//

#import "EVFCryptoHelper.h"
#import <CommonCrypto/CommonDigest.h>
#define kSalt @"872345628765923765"

@implementation EVFCryptoHelper

+(NSString *)hashedValueForString:(NSString*)stringString
{
    const int HASH_SIZE = 32;
    unsigned char hashedChars[HASH_SIZE];
    const char *string = [stringString UTF8String];
    size_t stringLen = strlen(string);
    // Confirm that the length of the user name is small enough
    // to be recast when calling the hash function.
    if (stringLen > UINT32_MAX) {
        //NSLog(@"Account name too long to hash: %@", stringString);
        return nil;
    }
    CC_SHA256(string, (CC_LONG)stringLen, hashedChars);
    // Convert the array of bytes into a string showing its hex representation.
    NSMutableString *stringHash = [[NSMutableString alloc] init];
    for (int i = 0; i < HASH_SIZE; i++) {
        [stringHash appendFormat:@"%02x", hashedChars[i]];
    }
    return stringHash;
}


+(NSString*)passKeyForUsername:(NSString *)username andPassword:(NSString *)password{

    NSString *salt = kSalt;
    NSString *hash = [EVFCryptoHelper hashedValueForString:[NSString stringWithFormat:@"%@%@%@",username,password,salt]];
    return hash;
    
}


+(NSString*)resetRequestKeyForUsername:(NSString *)username andSalt:(NSString *)salt{
    
    NSString *hash = [EVFCryptoHelper hashedValueForString:[NSString stringWithFormat:@"%@%@",username,salt]];
    return hash;
    
}


+(void)saveSessionKey:(NSString *)sessionKey{
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath  = [myPathList  objectAtIndex:0];
    NSError *error;
    NSString *filePath = [cachePath stringByAppendingPathComponent:@"CachedData.str"];
    [sessionKey writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if(error){
        ///NSLog(@"getFileData() - ERROR: %@",[error localizedDescription]);
    }

}


+(NSString *)loadSessionKey{
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath  = [myPathList  objectAtIndex:0];
    NSError *error;
    NSString *filePath = [cachePath stringByAppendingPathComponent:@"CachedData.str"];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSString *sessionKey = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        if(error){
            //NSLog(@"getFileData() - ERROR: %@",[error localizedDescription]);
        }
        return sessionKey;
    }
    
    return nil;
}

+(void)deleteSessionKey{
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath  = [myPathList  objectAtIndex:0];
    NSError *error;
    NSString *filePath = [cachePath stringByAppendingPathComponent:@"CachedData.str"];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        
    }

    


}


@end
