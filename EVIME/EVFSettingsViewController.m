//
//  EVFSettingsViewController.m
//  EVIME
//
//  Created by Evgeny on 17/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFSettingsViewController.h"
#import <StoreKit/StoreKit.h>
#import <MessageUI/MessageUI.h>
#import "EVFAppDelegate.h"
#import "EVFProperty.h"
#import "EVFLoadingProcessing.h"
#import "EVFViewController.h"


@interface EVFSettingsViewController ()<MFMailComposeViewControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@implementation EVFSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
    
}

- (void)reload {
   /*
    _products = nil;
    
    [[EVFIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
            
            SKProduct * product = (SKProduct *) _products[0];

            NSLog(@"product.localizedTitle: %@",product.localizedTitle);
            NSLog(@"product.price: %@",[_priceFormatter stringFromNumber:product.price]);
            
            
            self.productLabel.text = product.localizedTitle;
            [_priceFormatter setLocale:product.priceLocale];
            [self.buyButton setTitle:[_priceFormatter stringFromNumber:product.price] forState:UIControlStateNormal];
            
            if ([[EVFIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
                
                [self.buyButton setTitle:@"Done" forState:UIControlStateNormal];
                
            }
            
        }
        
    }];
    */
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /*
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    [self reload];
    */
    self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    self.fullNameTextField.text = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] userFullName];
    self.usernameLabel.text = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        self.infoLabel.hidden = YES;
    }else{
        self.fullNameTextField.hidden = YES;
        self.usernameLabel.hidden = YES;
        [self.logoutButton setTitle:@"Sign In or Create Account" forState:UIControlStateNormal];
    }
    
}


- (void)viewWillAppear:(BOOL)animated {
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productSuccessfullyPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
   // [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (IBAction)buy:(UIButton *)sender {
    
    SKProduct *product = _products[0];
    
    NSLog(@"Buying %@...", product.productIdentifier);
    [[EVFIAPHelper sharedInstance] buyProduct:product];
    
}

- (void)productSuccessfullyPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            
            NSLog(@"Product Purchased!");
            if ([[EVFIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
                
                [self.buyButton setTitle:@"Done" forState:UIControlStateNormal];
                
            }
            
            *stop = YES;
        }
    }];
    
}
- (IBAction)restore:(UIButton *)sender {
    
     [[EVFIAPHelper sharedInstance] restoreCompletedTransactions];
    
}
 */
- (IBAction)emailToSupport:(UIButton *)sender {
    
    [self sendEmail];
}


- (IBAction)goToSite:(UIButton *)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://www.enotekit.com"];
    
    [[UIApplication sharedApplication] openURL:url];
    
}

-(void)sendEmail{
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        
        [mailViewController setToRecipients:@[@"support@enotekit.com"]];
        
        NSString *body;
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
            body =  [NSString stringWithFormat:@"\nUser: %@",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
        }else{
            body = @"";
        }
        
        [mailViewController setMessageBody:body isHTML:NO];
        [mailViewController setSubject:@""];
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        
    } else {
        //NSLog(@"Device is unable to send email in its current state.");
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Device is unable to send email in its current state." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    NSString *feedbackMsg;
    UIAlertView* alert;
    Boolean alertShow = YES;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            feedbackMsg = @"Mail sending canceled";
            alertShow = NO;
            break;
        case MFMailComposeResultSaved:
            feedbackMsg = @"Mail saved";
            alertShow = NO;
            break;
        case MFMailComposeResultSent:
            feedbackMsg = @"Mail sent";
            alertShow = NO;
            break;
        case MFMailComposeResultFailed:
            feedbackMsg = @"Mail sending failed";
            break;
        default:
            feedbackMsg = @"Mail not sent";
            break;
    }
    if(alertShow){
        alert = [[UIAlertView alloc] initWithTitle:@"Mail" message:feedbackMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{


    

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFProperty"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"key" ascending:NO],nil];
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithFormat:@"key=='FullName'"];
    [fetchRequest setPredicate:predicate];
    EVFProperty *fullNameProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil].lastObject;
    
    if(!fullNameProperty){
        fullNameProperty = [NSEntityDescription insertNewObjectForEntityForName:@"EVFProperty" inManagedObjectContext:self.managedObjectContext];
    }
    fullNameProperty.value = textField.text;
    fullNameProperty.key = @"FullName";
    fullNameProperty.status = @"notuploaded";
    
    [self.managedObjectContext save:nil];
    
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setUserFullName:textField.text];
    [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([string isEqualToString:@""]){
        return YES;
    }
    
    NSString *text = [textField.text stringByAppendingString:string];
    
    if(text.length>36){
        return NO;
    }
    
    
    return YES;
}


- (IBAction)logout:(UIButton *)sender {
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsLogoutAction:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsLoginAction:YES];
        [self.navigationController popViewControllerAnimated:NO];
    }
    
}
@end
