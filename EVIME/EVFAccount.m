//
//  EVFAccount.m
//  EVIME
//
//  Created by Evgeny on 1/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFAccount.h"


@implementation EVFAccount

-(BOOL)isAccountDataValidForCreate{
    
    if([self.username isEqualToString:@""] || [self.email isEqualToString:@""] || [self.password isEqualToString:@""] || [self.confirmPassword isEqualToString:@""]){
        self.notValidMessage = @"Please fill in all fields.";
        return NO;
    }
    
    if(self.username.length<3 || self.username.length>20){
        
        self.notValidMessage = @"Username must be 3-20 characters.";
        return NO;
        
    }
    
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_-"] invertedSet];
    
    if ([self.username rangeOfCharacterFromSet:set].location != NSNotFound) {
        //NSLog(@"This string contains illegal characters");
        self.notValidMessage = @"Username contains illegal characters!";
        return NO;
    }
    
    
    NSString *emailRegex = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if (![emailTest evaluateWithObject: self.email]){
    
        self.notValidMessage = @"Please enter a valid email address.";
        return NO;
    
    }
    
    
    if(![self.password isEqualToString:self.confirmPassword]){
        self.notValidMessage = @"Your passwords do not match.";
        return NO;
    }
    
    
    
    if(self.password.length<6 || self.password.length>20){
        
        self.notValidMessage = @"Password must be 6-20 characters.";
        return NO;
        
    }
    
    if(self.fullName.length>250){
        
        self.notValidMessage = @"Full name is too long.";
        return NO;
        
    }
    
    return YES;
}

-(BOOL)isAccountDataValidForLogin{
    if([self.username isEqualToString:@""] || [self.password isEqualToString:@""]){
    
        self.notValidMessage = @"Please enter username and password.";
        return NO;
    
    }
    

    return YES;
}

-(BOOL)isAccountDataValidForResetPassword{
    if([self.username isEqualToString:@""]){
        
        self.notValidMessage = @"Please enter username.";
        return NO;
        
    }
    
    
    return YES;
}

@end
