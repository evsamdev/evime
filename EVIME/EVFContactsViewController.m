//
//  EVFContactsViewController.m
//  EVIME
//
//  Created by Evgeny on 27/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFContactsViewController.h"
#import "EVFContact.h"
#import "EVFAppDelegate.h"
#import <MessageUI/MessageUI.h>

@interface EVFContactsViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
@property (nonatomic, strong) NSArray *contactsList;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) BOOL isEditingMode;
@property (nonatomic) NSInteger selectedContactIndex;
@end

@implementation EVFContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)myInit
{
    //NSLog(@"Contacts Init");
    self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES],nil];
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = %@",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
        [fetchRequest setPredicate:predicate];
    }else{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = '_#_AnonimoussuominonA_#_'"];
        [fetchRequest setPredicate:predicate];
    }
    self.contactsList=[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    self.selectedContactIndex=-1;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self myInit];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [self.contactsTableView setEditing:NO];
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.contactsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EVFContact *contact = [self.contactsList objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"ContactCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.textLabel.text = contact.username;
    cell.detailTextLabel.text = contact.fullName;
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        EVFContact *contact = [self.contactsList objectAtIndex:indexPath.row];
        
        [self.managedObjectContext deleteObject:contact];
        
        NSError *error;
        [self.managedObjectContext save:&error];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
        fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES],nil];
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = %@",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
            [fetchRequest setPredicate:predicate];
        }else{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = '_#_AnonimoussuominonA_#_'"];
            [fetchRequest setPredicate:predicate];
        }
        self.contactsList=[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView endUpdates];
        self.contactsTableView.userInteractionEnabled = NO;
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.4];
    }
    
}

-(void)reloadTable{
    self.contactsTableView.userInteractionEnabled = YES;
    [self.contactsTableView reloadData];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    EVFContact *contact = [self.contactsList objectAtIndex:indexPath.row];
    
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Edit Contact" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.alertViewStyle =UIAlertViewStyleLoginAndPasswordInput;
    
    UITextField *usernameTextField = [alert textFieldAtIndex:0];
    usernameTextField.secureTextEntry = NO;
    usernameTextField.placeholder = @"Username";
    usernameTextField.text = contact.username;
    
    
    UITextField *fullnameTextField = [alert textFieldAtIndex:1];
    fullnameTextField.secureTextEntry = NO;
    fullnameTextField.placeholder = @"Full Name";
    fullnameTextField.text = contact.fullName;
    
    
    [alert show];
    self.selectedContactIndex = indexPath.row;
        
}

#pragma mark - Actions

- (IBAction)createNewContact:(UIBarButtonItem *)sender {
    
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"New Contact" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.alertViewStyle =UIAlertViewStyleLoginAndPasswordInput;
    
    UITextField *usernameTextField = [alert textFieldAtIndex:0];
    usernameTextField.secureTextEntry = NO;
    usernameTextField.placeholder = @"Username";
    
    
    UITextField *fullnameTextField = [alert textFieldAtIndex:1];
    fullnameTextField.secureTextEntry = NO;
    fullnameTextField.placeholder = @"Full Name";
    
    [alert show];
    self.selectedContactIndex = -1;
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex==1){
        
        NSError *error;
        UITextField *usernameTextField = [alertView textFieldAtIndex:0];
        UITextField *fullnameTextField = [alertView textFieldAtIndex:1];
        EVFContact *contact;
        if(self.selectedContactIndex == -1){
            contact  = [NSEntityDescription insertNewObjectForEntityForName:@"EVFContact" inManagedObjectContext:self.managedObjectContext];
            
        }else{
            
            contact = [self.contactsList objectAtIndex:self.selectedContactIndex];
        }
        contact.username = usernameTextField.text;
        contact.fullName = fullnameTextField.text;
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
            contact.user = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
        }else{
            contact.user = @"_#_AnonimoussuominonA_#_";
        }
        [self.managedObjectContext save:&error];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
        fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES],nil];
        
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = %@",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
            [fetchRequest setPredicate:predicate];
        }else{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = '_#_AnonimoussuominonA_#_'"];
            [fetchRequest setPredicate:predicate];
        }
        
        self.contactsList=[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        [self.contactsTableView reloadData];
        
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    
    UITextField *usernameTextField = [alertView textFieldAtIndex:0];
    if(usernameTextField.text.length==0){
        return NO;
    }
    return YES;
}


- (IBAction)close:(UIBarButtonItem *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)inviteUser:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Send invitation message." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email", nil];
    
    [actionSheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self sendSMS];
        //NSLog(@"0 button clicked");
    }else if (buttonIndex == 1) {
        [self sendEmail];
        //NSLog(@"1 button clicked");
    }else if (buttonIndex == 2) {
        //NSLog(@"2 button clicked");
    }

}

- (void)sendSMS{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
   
    NSString *message = [NSString stringWithFormat:
                         @"Hey, I started using Enotekit. Create multipage notes with text and photos and send to other users! - www.enotekit.com."];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    
    [messageController setBody:message];
    
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)sendEmail{
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        
        
        NSString *body;
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        body=  [NSString stringWithFormat:@"Hey,\nI started using Enotekit. It's an app that lets you create multipage notes with text and photos and send to other users.\nGet Enotekit: www.enotekit.com.\nYours, %@ (%@)",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] userFullName],[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
        }else{
             body=  [NSString stringWithFormat:@"Hey,\nI started using Enotekit. It's an app that lets you create multipage notes with text and photos and send to other users.\nGet Enotekit: www.enotekit.com."];
        }
        [mailViewController setSubject:@"Invitation to Enotekit"];
        [mailViewController setMessageBody:body isHTML:NO];
        
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        
    } else {
        //NSLog(@"Device is unable to send email in its current state.");
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Device is unable to send email in its current state." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    NSString *feedbackMsg;
    UIAlertView* alert;
    Boolean alertShow = YES;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            feedbackMsg = @"Mail sending canceled";
            alertShow = NO;
            break;
        case MFMailComposeResultSaved:
            feedbackMsg = @"Mail saved";
            alertShow = NO;
            break;
        case MFMailComposeResultSent:
            feedbackMsg = @"Mail sent";
            alertShow = NO;
            break;
        case MFMailComposeResultFailed:
            feedbackMsg = @"Mail sending failed";
            break;
        default:
            feedbackMsg = @"Mail not sent";
            break;
    }
    if(alertShow){
        alert = [[UIAlertView alloc] initWithTitle:@"Mail" message:feedbackMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
