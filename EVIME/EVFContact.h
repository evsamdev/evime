//
//  EVFContact.h
//  EVIME
//
//  Created by Evgeny on 29/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EVFContact : NSManagedObject

@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * user;

@end
