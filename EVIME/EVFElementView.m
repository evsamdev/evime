//
//  EVFElementView.m
//  EVIME
//
//  Created by Evgeny on 12.10.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFElementView.h"
#import "UIImage+KTCategory.h"

@implementation EVFElementView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)replaceControl{
    if(self.type == kElementTypeButton){
        UIView *actionControlView = self.controlView;
        actionControlView.frame = CGRectMake(self.frame.size.width-kResizeControlSide, self.frame.size.height/2-kResizeControlSide/2, kResizeControlSide, kResizeControlSide);
        //NSLog(@"replaceControl Action");
    }
  
}


-(void)setFrame:(CGRect)frame{
        
    [super setFrame:frame];
    [self replaceControl];
    [self resizeContentView];
   
 
}

-(void)resizeContentView{
    
    UIView *contentView = self.contentView;
       
    CGRect contentFrame = contentView.frame;
    
   // NSLog(@"self.frame.size.height: %f",self.frame.size.height);
    
    contentFrame.size.height = self.frame.size.height-10.0;
    contentView.frame = contentFrame;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
