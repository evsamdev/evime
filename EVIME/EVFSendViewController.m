//
//  EVFSendViewController.m
//  EVIME
//
//  Created by Evgeny on 17/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFSendViewController.h"
#import <MessageUI/MessageUI.h>
#import "EVFContact.h"
#import "EVFAppDelegate.h"
#import "EVFProperty.h"
#import "EVFLoadingProcessing.h"
#import "EVFEvimesListViewController.h"
#import "SBJson.h"
#import "EVFImage.h"
#import "EVFEmailGenerator.h"
#import "EVFLogNote.h"
#import "EVFLogProcessing.h"

@interface EVFSendViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
@property (nonatomic, strong) NSArray *contactsList;
@property (nonatomic, strong) NSArray *contactsListFiltered;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) BOOL isEditingMode;
@property (nonatomic) BOOL isSendLocked;
@property (nonatomic) NSInteger selectedContactIndex;
@property (nonatomic, strong) NSString *userFrom;
@property (nonatomic, strong) NSString *userTo;
@end

@implementation EVFSendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES],nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user = %@",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
    [fetchRequest setPredicate:predicate];
    self.contactsList=[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    self.selectedContactIndex=-1;
    self.contactsListFiltered = [self.contactsList copy];
    
    self.evimeTitleTextField.text = self.evimeToSend.title;
    self.myUsernameLabel.text = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    self.userFrom = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    self.myFullNameLabel.text = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] userFullName];
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TextField Delegate


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag==22){
        
        self.fullNameTextField.text = @"";
                
    }
    
    
    if(textField.tag == 21){
    
        
        if([string isEqualToString:@""]){
            return YES;
        }
        
        NSString *text = [textField.text stringByAppendingString:string];
        
        if(text.length>250){
            return NO;
        }
       
    
    }
    
    
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    
    if(textField.tag==21){
        
        self.evimeToSend.title = textField.text;
        NSError *error;
        [self.managedObjectContext save:&error];
    
    }
    
    return YES;
}


#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.contactsListFiltered.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EVFContact *contact = [self.contactsListFiltered objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"ContactCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.textLabel.text = contact.username;
    cell.detailTextLabel.text = contact.fullName;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EVFContact *contact = [self.contactsListFiltered objectAtIndex:indexPath.row];
    self.usernameTextField.text = contact.username;
    self.fullNameTextField.text = contact.fullName;
    self.userTo = contact.username;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Actions


- (IBAction)cancel:(UIBarButtonItem *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)send:(UIBarButtonItem *)sender {
    
    if(self.isSendLocked){
        return;
    }
    
    if([self.usernameTextField.text isEqualToString:@""]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter username" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    
    NSString *deviceUUID = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    NSString *sessionKey = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey];
    NSString *username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/check_user.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    NSString* str = [NSString stringWithFormat:@"sessionKey=%@&deviceUUID=%@&username=%@&user=%@",sessionKey,deviceUUID,username,self.usernameTextField.text];
    
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self.activityIndicator startAnimating];
    self.isSendLocked = YES;
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               [self.activityIndicator stopAnimating];
                               self.isSendLocked = NO;
                               
                               if(error){
                                   
                                   //NSLog(@"check_user.request error: %@", error.localizedDescription);
                                   [self finalSend];
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"check_user.responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];

                                   if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                       
                                       [self finalSend];
                                       
                                       NSString *userFullName = [responseDict objectForKey:@"fullName"];
                                       self.evimeToSend.userFullTo = userFullName;
                                       
                                   }else if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:-4]]){
                                       
                                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User with that username does not exist." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                       [alert show];
                                       
                                   
                                   }
    
    
                               }
                           }];
    
    
}


-(void)finalSend{
    
    self.evimeToSend.userFrom = self.userFrom;
    self.evimeToSend.userTo = self.usernameTextField.text;
    self.evimeToSend.box = @"sent";
    self.evimeToSend.sendDate = [NSDate date];
    self.evimeToSend.title = self.evimeTitleTextField.text;
    self.evimeToSend.status = @"notuploaded";
    self.evimeToSend.trash = [NSNumber numberWithInt:0];
    self.evimeToSend.readyness = [NSNumber numberWithInteger:0];
    
    NSString *UUID = [[NSUUID UUID] UUIDString];
    
    EVFEvime *newEvime = [NSEntityDescription insertNewObjectForEntityForName:@"EVFEvime" inManagedObjectContext:self.managedObjectContext];
    newEvime.user = self.usernameTextField.text;
    newEvime.box = @"inbox";
    newEvime.body = self.evimeToSend.body;
    newEvime.uuid = UUID;
    newEvime.createDate = self.evimeToSend.createDate;
    newEvime.index = nil;
    newEvime.trash = [NSNumber numberWithInt:0];
    newEvime.title = self.evimeToSend.title;
    newEvime.group = @"main";
    newEvime.lastEditDate = self.evimeToSend.lastEditDate;
    newEvime.sendDate = [NSDate date];
    newEvime.userFrom = self.userFrom;
    newEvime.userTo = self.usernameTextField.text;
    newEvime.readyness = [NSNumber numberWithInteger:0];
    newEvime.sourceUUID = self.evimeToSend.uuid;
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"evimeUUID = %@",self.evimeToSend.uuid];
    [fetchRequest setPredicate:predicate];
    NSArray *imageList =[self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    BOOL isAllUploaded=YES;
    for(EVFImage *image in imageList){
        
        if([image.status isEqualToString:@"notuploaded"]){
            isAllUploaded=NO;
        }
        
    }
    
    if(isAllUploaded){
        
        newEvime.status = @"notuploaded";
    }
    
    [self.managedObjectContext save:&error];

    [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setDestinationBox:@"sent"];
    NSArray *controllers = [self.navigationController childViewControllers];
    for(UIViewController *vc in controllers){
        if([vc isKindOfClass:[EVFEvimesListViewController class]]){
            
            [self.navigationController popToViewController:vc animated:YES];
        }
        
    }
    
}


#pragma mark - Message Sending

- (IBAction)inviteUser:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Send invitation message." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email", nil];
    
    [actionSheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self sendSMS];
        //NSLog(@"0 button clicked");
    }else if (buttonIndex == 1) {
        [self sendEmail];
        //NSLog(@"1 button clicked");
    }else if (buttonIndex == 2) {
        //NSLog(@"2 button clicked");
    }
    
}


- (void)sendSMS{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    
    NSString *message = [NSString stringWithFormat:
                         @"Hey, I started using Enotekit. Create multipage notes with text and photos and send to other users! - www.enotekit.com."];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    
    [messageController setBody:message];
    
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)sendEmail{
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        
         NSString *body =  [NSString stringWithFormat:@"Hey,\nI started using Enotekit. It's an app that lets you create multipage notes with text and photos and send to other users.\nGet Enotekit: www.enotekit.com.\nYours, %@ (%@)",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] userFullName],[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
        
        [mailViewController setMessageBody:body isHTML:NO];
        [mailViewController setSubject:@"Invitation to Enotekit"];
        [self presentViewController:mailViewController animated:YES completion:nil];
        
    } else {
        //NSLog(@"Device is unable to send email in its current state.");
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Device is unable to send email in its current state." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    NSString *feedbackMsg;
    UIAlertView* alert;
    Boolean alertShow = YES;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            feedbackMsg = @"Mail sending canceled";
            alertShow = NO;
            break;
        case MFMailComposeResultSaved:
            feedbackMsg = @"Mail saved";
            alertShow = NO;
            break;
        case MFMailComposeResultSent:{
            feedbackMsg = @"Mail sent";
            alertShow = NO;
            EVFLogNote *logNote = [[EVFLogNote alloc] init];
            logNote.eventId = 100;
            logNote.note = self.evimeToSend.uuid;
            logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
            [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
            break;
        }
        case MFMailComposeResultFailed:
            feedbackMsg = @"Mail sending failed";
            break;
        default:
            feedbackMsg = @"Mail not sent";
            break;
    }
    
    //NSLog(@"feedbackMsg: %@",feedbackMsg);
        
    if(alertShow){
        alert = [[UIAlertView alloc] initWithTitle:@"Mail" message:feedbackMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)sendByEmail:(UIButton *)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        
        NSString *body = [[[EVFEmailGenerator alloc] init] htmlEmailBodyForEvime:self.evimeToSend];
        [mailViewController setSubject:self.evimeTitleTextField.text];
        [mailViewController setMessageBody:body isHTML:YES];
        [self presentViewController:mailViewController animated:YES completion:nil];
        
    } else {
        //NSLog(@"Device is unable to send email in its current state.");
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Device is unable to send email in its current state." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

    
}
@end
