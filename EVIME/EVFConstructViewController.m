//
//  EVFConstructViewController.m
//  EVIME
//
//  Created by Evgeny on 30.09.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFConstructViewController.h"
#import "EVFElementView.h"
#import "UIImage+KTCategory.h"
#import "EVFReplayViewController.h"
#import "EVFAppDelegate.h"
#import "UIImagePickerController+UIImagePickerController_NonRotating.h"
#import "SBJson.h"
#import "EVFEvimesListViewController.h"
#import "EVFImage.h"
#import "EVFLoadingProcessing.h"

#define DegreesToRadian(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)

@interface EVFConstructViewController ()

@property (nonatomic) CGPoint beginPoint;
@property (nonatomic, strong) NSMutableArray *elementsList;
@property (nonatomic, strong) EVFElementView *activeElementView;
@property (nonatomic) BOOL isAnyElementIsactive;
@property (nonatomic) BOOL isLockedToAddNewElement;
@property (nonatomic) BOOL isBottomOfScreenReached;
@property (nonatomic) BOOL isEditingMode;
@property (nonatomic) BOOL isTableScaled;
@property (nonatomic) BOOL isNeedToScrollEditedElement;
@property (nonatomic) BOOL isTextEditingMethod;
@property (nonatomic) BOOL isNewElement;
@property (nonatomic) BOOL isNewImage;
@property (nonatomic, strong) NSTimer *bottomScrollTimer;
@property (nonatomic, strong) UITextView *activeTextView;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) NSMutableArray *elementsDataList;
@property (nonatomic, strong) NSMutableDictionary *activeContentData;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) NSInteger tableSqueezeHeight;
@end

@implementation EVFConstructViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSString *version = [[UIDevice currentDevice] systemVersion];
    NSInteger ver = [version integerValue];
    //NSLog(@"iOS version: %ld",(long)ver);
    if (ver < 8){
        self.tableSqueezeHeight = 164;
    }else{
        self.tableSqueezeHeight = 204;
    }

    
    
    if(self.pageNumber==1){
        [self.navigationItem setHidesBackButton:YES];
    }
    self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    self.elementsList = [[NSMutableArray alloc] init];
        
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    self.constructTableView.backgroundColor = [UIColor clearColor];
    UIButton *addImageElementButton = [[UIButton alloc] initWithFrame:CGRectMake(6, 3, 50, 50)];
    addImageElementButton.tag = 101;
    [addImageElementButton setBackgroundImage:[UIImage imageNamed:@"iconImage.png"] forState:UIControlStateNormal];
    [addImageElementButton addTarget:self action:@selector(addNewElement:) forControlEvents:UIControlEventTouchUpInside];
    addImageElementButton.layer.cornerRadius = 10;
    addImageElementButton.layer.masksToBounds = YES;
    [self.elementsSourceScrollVew addSubview:addImageElementButton];
    
    
    UIButton *addTextElementButton = [[UIButton alloc] initWithFrame:CGRectMake(62, 3, 50, 50)];
    addTextElementButton.tag = 102;
    [addTextElementButton setBackgroundImage:[UIImage imageNamed:@"iconText.png"] forState:UIControlStateNormal];
    [addTextElementButton addTarget:self action:@selector(addNewElement:) forControlEvents:UIControlEventTouchUpInside];
    addTextElementButton.layer.cornerRadius = 10;
    addTextElementButton.layer.masksToBounds = YES;
    [self.elementsSourceScrollVew addSubview:addTextElementButton];
    
    UIButton *addActiveTextElementButton = [[UIButton alloc] initWithFrame:CGRectMake(118, 3, 50, 50)];
    addActiveTextElementButton.tag = 103;
    [addActiveTextElementButton setBackgroundImage:[UIImage imageNamed:@"iconActive.png"] forState:UIControlStateNormal];
    [addActiveTextElementButton addTarget:self action:@selector(addNewElement:) forControlEvents:UIControlEventTouchUpInside];
    addActiveTextElementButton.layer.cornerRadius = 10;
    addActiveTextElementButton.layer.masksToBounds = YES;
    [self.elementsSourceScrollVew addSubview:addActiveTextElementButton];
    
    UIButton *addBlankElementButton = [[UIButton alloc] initWithFrame:CGRectMake(174, 3, 50, 50)];
    addBlankElementButton.tag = 104;
    [addBlankElementButton setBackgroundImage:[UIImage imageNamed:@"iconHeader.png"] forState:UIControlStateNormal];
    [addBlankElementButton addTarget:self action:@selector(addNewElement:) forControlEvents:UIControlEventTouchUpInside];
    addBlankElementButton.layer.cornerRadius = 10;
    addBlankElementButton.layer.masksToBounds = YES;
    [self.elementsSourceScrollVew addSubview:addBlankElementButton];
    
    self.elementsDataList = [self.pageData objectForKey:@"elements"];
    
    if(!self.elementsDataList){
        self.elementsDataList = [[NSMutableArray alloc] init];
        [self.pageData setObject:self.elementsDataList forKey:@"elements"];
    }
    
    //NSLog(@"elementsDataList: %@", self.elementsDataList);
    
    for(NSDictionary *contentData in self.elementsDataList){
        
        ElementType type = [[contentData objectForKey:@"type"] integerValue];
        float height = [[contentData objectForKey:@"height"] floatValue];
        if(height==0){
            height = 50.0;
        }
        
        CGRect newElementFrame;
        UIView *contentView;
        float aspectRatio;
        switch (type) {
            case kElementTypeImage:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                
                NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *myDocsPath    = [myPathList  objectAtIndex:0];
                NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:self.currentEvime.uuid];
                NSString *imageFile = [currentImagesDir stringByAppendingPathComponent:[NSString stringWithFormat:@"th_%@",[contentData objectForKey:@"content"]]];
                UIImage *contentImage = [UIImage imageWithContentsOfFile:imageFile];
                float imageHeight = height-10;
                float imageX = 0;
                float imageWidth = 280;
                
                if(contentImage){
                    
                    if(contentImage.size.height>contentImage.size.width){
                        
                        aspectRatio = contentImage.size.height/contentImage.size.width;
                        
                        if(imageHeight>280){
                            imageHeight = 280;
                        }
                        
                        imageWidth = imageHeight/aspectRatio;
                        imageX =(280-imageWidth)/2.0;
                        
                    }else{
                        aspectRatio = contentImage.size.width/contentImage.size.height;
                        
                        float maxImageHeight = 280/aspectRatio;
                        
                        if(imageHeight>maxImageHeight){
                            imageHeight = maxImageHeight;
                        }
                        
                        imageWidth = imageHeight*aspectRatio;
                        imageX= (280-imageWidth)/2.0;
                        
                    }
                    
                    UIImageView *imageContentView = [[UIImageView alloc] initWithFrame:CGRectMake(imageX, 10, imageWidth, imageHeight)];
                    imageContentView.userInteractionEnabled = YES;
                    contentView = imageContentView;
                    imageContentView.image =  contentImage;
                }
                UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImageForElement:)];
                [contentView addGestureRecognizer:tapGR];
                
                break;
            }
                
            case kElementTypeText:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                UITextView *textContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, height-10)];
                textContentView.backgroundColor = [UIColor clearColor];
                textContentView.returnKeyType = UIReturnKeyDefault;
                textContentView.font=[UIFont systemFontOfSize:16];
                textContentView.delegate = self;
                textContentView.scrollEnabled = NO;
                contentView = textContentView;
                textContentView.text = [contentData objectForKey:@"content"];
                break;
            }
                
            case kElementTypeButton:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                UITextView *buttonContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, height-10)];
                buttonContentView.backgroundColor = [UIColor clearColor];
                buttonContentView.returnKeyType = UIReturnKeyDefault;
                buttonContentView.font=[UIFont boldSystemFontOfSize:16];
                buttonContentView.textAlignment = NSTextAlignmentCenter;
                buttonContentView.textColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:1.0];
                buttonContentView.delegate = self;
                buttonContentView.scrollEnabled = NO;
                contentView = buttonContentView;
                buttonContentView.text = [contentData objectForKey:@"content"];
                break;
            }
                
            case kElementTypeHeader:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                UITextView *textContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, height-10)];
                textContentView.backgroundColor = [UIColor clearColor];
                textContentView.textAlignment = NSTextAlignmentCenter;
                textContentView.returnKeyType = UIReturnKeyDefault;
                textContentView.font=[UIFont boldSystemFontOfSize:16];
                textContentView.delegate = self;
                textContentView.scrollEnabled = NO;
                contentView = textContentView;
                textContentView.text = [contentData objectForKey:@"content"];
                break;
            }
                
            default:
                break;
                
        }
        EVFElementView  *elementNextView = [[EVFElementView alloc] initWithFrame:newElementFrame];
        elementNextView.backgroundColor = [UIColor clearColor];
        elementNextView.type = type;
        elementNextView.aspectRatio = aspectRatio;
        [elementNextView addSubview:contentView];
        [self.elementsList addObject:elementNextView];
        elementNextView.contentView = contentView;
        CGRect parentFrame = elementNextView.frame;
        if(type==kElementTypeButton){
            
            UIView *actionControlView = [[UIView alloc] initWithFrame:CGRectMake(parentFrame.size.width-kResizeControlSide, parentFrame.size.height/2-kResizeControlSide/2, kResizeControlSide, kResizeControlSide)];
            actionControlView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"arrow.png"]];
            actionControlView.alpha = 0.7;
            
            UITapGestureRecognizer *tapGRForButton = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionForButton:)];
            [actionControlView addGestureRecognizer:tapGRForButton];
            [elementNextView addSubview:actionControlView];
            elementNextView.controlView = actionControlView;
            
            
        }
    }

    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setEvimeForEdit:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    [self.constructTableView setEditing:NO];
    
}


- (void)addNewElement:(UIButton *)sender {
    
    if(self.isLockedToAddNewElement){
        return;
    }
    
    if(self.isEditingMode){
        [self editMode:nil];
    }
    
    NSMutableDictionary *contentData = [[NSMutableDictionary alloc] init];
       
    CGRect newElementFrame;
    UIView *contentView;
    ElementType type;
    UITapGestureRecognizer *tapGR;
    switch (sender.tag) {
        
        case 101:{
            newElementFrame = CGRectMake(20, 0, 280, 220);
            UIImageView *imageContentView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 280, 210)];
            imageContentView.userInteractionEnabled = YES;
            contentView = imageContentView;
            tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImageForElement:)];
            [contentView addGestureRecognizer:tapGR];
            type = kElementTypeImage;
            self.isNewImage = YES;
            break;
        }
           
        case 102:{
            newElementFrame = CGRectMake(20, 0, 280, 46);
            UITextView *textContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, 30)];
            textContentView.backgroundColor = [UIColor clearColor];
            textContentView.delegate = self;
            textContentView.returnKeyType = UIReturnKeyDefault;
            textContentView.font=[UIFont systemFontOfSize:16];
            textContentView.scrollEnabled = NO;
            contentView = textContentView;
            type = kElementTypeText;
            break;
        }
            
        case 103:{
            newElementFrame = CGRectMake(20, 0, 280, 46);
            UITextView *buttonContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, 30)];
            buttonContentView.delegate = self;
            buttonContentView.backgroundColor = [UIColor clearColor];
            buttonContentView.returnKeyType = UIReturnKeyDefault;
            buttonContentView.font=[UIFont boldSystemFontOfSize:16];
            buttonContentView.textAlignment = NSTextAlignmentCenter;
            buttonContentView.textColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:1.0];
            buttonContentView.scrollEnabled = NO;
            contentView = buttonContentView;
            type = kElementTypeButton;
            break;
        }
        
        case 104:{
            newElementFrame = CGRectMake(20, 0, 280, 46);
            UITextView *textContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, 30)];
            textContentView.backgroundColor = [UIColor clearColor];
            textContentView.delegate = self;
            textContentView.returnKeyType = UIReturnKeyDefault;
            textContentView.font=[UIFont boldSystemFontOfSize:16];
            textContentView.textAlignment = NSTextAlignmentCenter;
            textContentView.scrollEnabled = NO;
            contentView = textContentView;
            type = kElementTypeHeader;
            break;
        }
                  
        default:
            type = kElementTypeHeader;
            break;
    }
    
    
    [contentData setObject:[NSNumber numberWithInteger:type] forKey:@"type"];
    
    EVFElementView  *elementNextView = [[EVFElementView alloc] initWithFrame:newElementFrame];//CGRectMake(20, 0, 280, 50)];
    elementNextView.type = type;
    [elementNextView addSubview:contentView];
    
    elementNextView.backgroundColor = [UIColor clearColor];;
    
    CGRect parentFrame = elementNextView.frame;
    
    if(type==kElementTypeButton){
        
        UIView *actionControlView = [[UIView alloc] initWithFrame:CGRectMake(parentFrame.size.width-kResizeControlSide, parentFrame.size.height/2-kResizeControlSide/2, kResizeControlSide, kResizeControlSide)];
        actionControlView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"arrow.png"]];
        actionControlView.alpha = 0.7;
        
        UITapGestureRecognizer *tapGRForButton = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionForButton:)];
        [actionControlView addGestureRecognizer:tapGRForButton];
        [elementNextView addSubview:actionControlView];
        elementNextView.controlView = actionControlView;
        
    }
    
    elementNextView.contentView = contentView;
    [self.elementsList addObject:elementNextView];
    
    NSInteger newIndex = [self.elementsList indexOfObject:elementNextView];
    [contentData setObject:[NSNumber numberWithInteger:newIndex] forKey:@"index"];
    
    [self.elementsDataList addObject:contentData];
    
    [self.constructTableView reloadData];
    self.isNewElement = YES;
    [self.constructTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:self.elementsList.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    
    EVFElementView *lastElementView = self.elementsList.lastObject;
    UIView *lastContentView = lastElementView.contentView;
    
    if([lastContentView isKindOfClass:[UIImageView class]]){
        
        [self selectImageForElement:tapGR];
    
    }
    
    if([lastContentView isKindOfClass:[UITextView class]]){
        
         [self performSelector:@selector(activateLastElement:) withObject:lastContentView afterDelay:0.3];
    }
   
    //NSLog(@"self.pageFullData : %@",self.bodyData );
    
    self.currentEvime.body = [self.bodyData  JSONRepresentation];
    
    NSError *error;
    [self.managedObjectContext save:&error];
    
}

-(void)activateLastElement:(UITextView *)lastContentView{
    if(self.isNewElement){
        //NSLog(@"activateLastElement");
        [lastContentView becomeFirstResponder];
        self.isNewElement = NO;
    }


}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    if(self.isNewElement){
        // NSLog(@"scrollViewDidEndScrollingAnimation");
        EVFElementView *lastElementView = self.elementsList.lastObject;
        
        UIView *lastContentView = lastElementView.contentView;
        if([lastContentView isKindOfClass:[UITextView class]]){
            [lastContentView becomeFirstResponder];
            
        }
        self.isNewElement = NO;
    }

}


- (IBAction)editMode:(UIButton *)sender {
    
    self.isEditingMode = !self.isEditingMode;
    if(self.isEditingMode){
        [self.constructTableView setEditing:YES animated:YES];
        //[self.editModeButton setTitle:@"Done" forState:UIControlStateNormal];
        
        for(EVFElementView *elementView in self.elementsList){
        
            UIView *resizeControlView = elementView.controlView;
            resizeControlView.hidden = YES;
        
        }
        
        
    }else{
        [self.constructTableView setEditing:NO animated:YES];
        //[self.editModeButton setTitle:@"" forState:UIControlStateNormal];
        
        for(EVFElementView *elementView in self.elementsList){
            
            UIView *resizeControlView = elementView.controlView;
            resizeControlView.hidden = NO;
            
        }

    
    }
    
    
}

#pragma mark - Actions

-(void)actionForButton:(UITapGestureRecognizer *)sender{

    //NSLog(@"actionForButton");
   
    self.activeElementView = (EVFElementView *)[sender.view superview];
    self.activeContentData = [self.elementsDataList objectAtIndex:[self.elementsList indexOfObject:self.activeElementView]];
    
    if(!self.activeElementView.childConstructViewcontroller){
        
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        NSMutableDictionary *newPageData = [self.activeContentData objectForKey:@"page"];
        
        if(![self.activeContentData objectForKey:@"page"]){
            
            newPageData = [[NSMutableDictionary alloc] init];
            [self.activeContentData setValue:newPageData forKey:@"page"];
        }
        
        EVFConstructViewController * constructViewController = (EVFConstructViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFConstruct"];
        constructViewController.title = ((UITextView *)self.activeElementView.contentView).text;
        constructViewController.pageData = newPageData;
        constructViewController.bodyData = self.bodyData;
        constructViewController.currentEvime = self.currentEvime;
        constructViewController.pageNumber = self.pageNumber+1;
        self.activeElementView.childConstructViewcontroller = constructViewController;
        //NSLog(@"self.pageFullData : %@",self.bodyData );
        
        self.currentEvime.body = [self.bodyData  JSONRepresentation];
        
        NSError *error;
        [self.managedObjectContext save:&error];
            
        
    }
    self.activeElementView.childConstructViewcontroller.navigationItem.title = ((UITextView *)self.activeElementView.contentView).text;
    [self.self.navigationController pushViewController:self.activeElementView.childConstructViewcontroller animated:YES];
    

}

-(IBAction)replay:(UIBarButtonItem *)sender {
    
    
    if(self.isTextEditingMethod){
        
        [self.activeTextView resignFirstResponder];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.isTableScaled = NO;
            CGRect tableFrame = self.constructTableView.frame;
            tableFrame.size.height+=self.tableSqueezeHeight;
            self.constructTableView.frame = tableFrame;
            
        }];
        return;
    }
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    }
    
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    
    EVFReplayViewController * replayViewController = (EVFReplayViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFReplay"];
    replayViewController.currentEvime = self.currentEvime;
    replayViewController.pageData = [self.bodyData objectForKey:@"page"];
    replayViewController.title = self.currentEvime.title;
    replayViewController.isFirstPage = YES;
    [self.navigationController pushViewController:replayViewController animated:YES];
    
    
}

- (IBAction)closeEvime:(UIButton *)sender {
    
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setDestinationBox:@"draft"];
    
    NSArray *controllers = [self.navigationController childViewControllers];
    
    for(UIViewController *vc in controllers){
        if([vc isKindOfClass:[EVFEvimesListViewController class]]){
            
            [self.navigationController popToViewController:vc animated:YES];
        }
        
    }
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    }
    
}

#pragma mark - Image Element

-(void)selectImageForElement:(UITapGestureRecognizer *)sender{

    //NSLog(@"selectImageForElement");
    if(self.isEditingMode){
        return;
    }

    
    if(self.isTextEditingMethod){
        
        [self.activeTextView resignFirstResponder];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.isTableScaled = NO;
            CGRect tableFrame = self.constructTableView.frame;
            tableFrame.size.height+=self.tableSqueezeHeight;
            self.constructTableView.frame = tableFrame;
            
        }];
       
    }

    
    [[self imagePicker] setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:self.imagePicker animated:YES completion:nil];

    self.activeElementView = (EVFElementView *)[sender.view superview];
    self.activeContentData = [self.elementsDataList objectAtIndex:[self.elementsList indexOfObject:self.activeElementView]];
    

}


- (UIImagePickerController *)imagePicker {
    if (_imagePicker) {
        return _imagePicker;
    }
    
    _imagePicker = [[UIImagePickerController alloc] init];
    [_imagePicker setDelegate:self];
    [_imagePicker setAllowsEditing:NO];
    return _imagePicker;
}


#pragma mark - TextView delegate


- (void)setCursorToEnd:(UITextView *)inView
{
    [UIView animateWithDuration:0.4 animations:^{
        
        inView.selectedRange = NSMakeRange(inView.text.length, 0);
        inView.contentOffset = CGPointMake(0, inView.contentSize.height-inView.frame.size.height);
    }];
    
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{

    if(self.isEditingMode){
        return NO;
    }
    
    return YES;
}


- (void) textViewDidBeginEditing:(UITextView *)textView{
    self.isTextEditingMethod = YES;
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(replay:)] ;
    self.isNeedToScrollEditedElement = NO;
    self.activeTextView = textView;
    self.activeElementView = (EVFElementView *)[textView superview];
    self.activeContentData = [self.elementsDataList objectAtIndex:[self.elementsList indexOfObject:self.activeElementView]];
    
    for(EVFElementView *elementView in self.elementsList){
        
        UIView *resizeControlView = elementView.controlView;
        resizeControlView.hidden = YES;
        
    }
    
    if(!self.isTableScaled){
        
        self.isTableScaled = YES;
         __block CGRect tableFrame = self.constructTableView.frame;
        tableFrame.size.height -=self.tableSqueezeHeight;
        [UIView animateWithDuration:0.4 animations:^{
            
                //(textFrame.origin.y+textFrame.size.height)-(self.view.frame.size.height-200)+20;
                self.constructTableView.frame = tableFrame;
            
            } completion:^(BOOL copmp){
                
           
                [self.constructTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:[self.elementsList indexOfObject:self.activeElementView] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
                
                
                [self performSelector:@selector(setCursorToEnd:) withObject:textView afterDelay:0.2];
               
        
        }];
        
                
        
        
    }else{
        
        [self.constructTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:[self.elementsList indexOfObject:self.activeElementView] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
        
        
        [self performSelector:@selector(setCursorToEnd:) withObject:textView afterDelay:0.2];
    
    }
    
    
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    CGSize editedStringSize;
    
    if(self.activeElementView.type ==  kElementTypeText){
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:16] forKey: NSFontAttributeName];
        
        editedStringSize = [[textView.text substringToIndex:range.location] boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                                                   options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:stringAttributes context:nil].size;
        
    }else{
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:16] forKey: NSFontAttributeName];
        
        editedStringSize = [[textView.text substringToIndex:range.location] boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                                                   options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:stringAttributes context:nil].size;
        
    }
    
    if((textView.frame.size.height-20) - editedStringSize.height < self.constructTableView.frame.size.height-30){
        
        //NSLog(@"shouldChangeTextInRange: isNeedToScrollEditedElement");
        
        self.isNeedToScrollEditedElement = YES;
    }else{
    
        self.isNeedToScrollEditedElement = NO;
    }
    /*
    if([text isEqualToString:@"\n"] || [text isEqualToString:@""] ){
        self.isNeedToScrollEditedElement = NO;
    }
    */
    
    return YES;
}


- (void) textViewDidChange:(UITextView *)textView{
    
    CGSize stringSize;
    
    NSString *textToMeasure= [textView.text copy];
    
    if(textView.text.length==0){
        
        textToMeasure=@"";
        
    }else{
        NSString *lastChar = [textToMeasure substringFromIndex:textView.text.length-1];
      
        if([lastChar isEqualToString:@"\n"]){
            
            textToMeasure = [textToMeasure stringByAppendingString:@"-"];
        }
    }
    if(self.activeElementView.type ==  kElementTypeText){
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:16] forKey: NSFontAttributeName];
        
        stringSize = [textToMeasure boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                 options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                              attributes:stringAttributes context:nil].size;
        
    }else{
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:16] forKey: NSFontAttributeName];
        
        stringSize = [textToMeasure boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                 options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                              attributes:stringAttributes context:nil].size;
        
    }
    //NSLog(@"textViewDidChange: stringSize.height : %f",stringSize.height );
    CGRect frame = textView.frame;
    
    frame.size.height = stringSize.height+20;
    textView.frame = frame;
    
    [self.constructTableView beginUpdates];
    [self.constructTableView endUpdates];
    
    if(self.isNeedToScrollEditedElement){
        [self.constructTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:[self.elementsList indexOfObject:self.activeElementView] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}


-(void) textViewDidEndEditing:(UITextView *)textView{
    self.activeTextView=nil;
    self.isTextEditingMethod = NO;
    
    NSString *textToMeasure=[textView.text copy];
    
    if(textView.text.length==0){
    
        textToMeasure=@"";
    
    }else{
        
        NSString *lastChar = [textToMeasure substringFromIndex:textView.text.length-1];
        
        if([lastChar isEqualToString:@"\n"]){
            
            textToMeasure = [textToMeasure stringByAppendingString:@"-"];
        }
    }
     self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(replay:)] ;
    
    CGRect frame  = self.activeElementView.frame;
    
    CGSize stringSize;
    
    if(self.activeElementView.type ==  kElementTypeText){
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:16] forKey: NSFontAttributeName];
        
        stringSize = [textToMeasure boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                 options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                              attributes:stringAttributes context:nil].size;
        
        
    }else{
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:16] forKey: NSFontAttributeName];
        
        stringSize = [textToMeasure boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                 options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                              attributes:stringAttributes context:nil].size;
        
    }
    //NSLog(@"textView.text: %@", textView.text);
    //NSLog(@"stringSize.height+28: %f",stringSize.height+27);
    
    frame.size.height = stringSize.height+27;//textView.contentSize.height+10;
    self.activeElementView.frame = frame;
    
    for(EVFElementView *elementView in self.elementsList){
        
        UIView *resizeControlView = elementView.controlView;
        resizeControlView.hidden = NO;
        
    }
    
    [self.activeContentData setObject:textView.text forKey:@"content"];
    [self.activeContentData setObject:[NSNumber numberWithFloat:self.activeElementView.frame.size.height] forKey:@"height"];
    
   // NSLog(@"self.activeElementView.frame: %f", self.activeElementView.frame.size.height);
    //NSLog(@"textViewDidEndEditing");
    
    self.currentEvime.body = [self.bodyData  JSONRepresentation];
    self.currentEvime.status = @"notuploaded";
    self.currentEvime.lastEditDate = [NSDate date];
    NSError *error;
    [self.managedObjectContext save:&error];
    //[[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    
    [self.constructTableView beginUpdates];
    [self.constructTableView endUpdates];
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
     return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
     return self.elementsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EVFElementView *elementView = [self.elementsList objectAtIndex:indexPath.row];
    elementView.tag = 1001;
    static NSString *CellIdentifier = @"ElementCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell.contentView addSubview:elementView];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
    }else{
        
        EVFElementView *elementViewInCell =  (EVFElementView *)[cell.contentView viewWithTag:1001];
        
        [elementViewInCell removeFromSuperview];
        [cell.contentView addSubview:elementView];
        
    }
   
    return cell;
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
     return YES;
 }



 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         
         if(!self.isEditingMode){
             self.constructTableView.editing = NO;
         }
         NSDictionary *elementData = [self.elementsDataList objectAtIndex:indexPath.row];
         
         NSInteger type = [[elementData objectForKey:@"type"] integerValue];
         
         if(type==kElementTypeImage){
             
             NSString *imageFileName = [elementData objectForKey:@"content"];
             NSString *evimeUuid = self.currentEvime.uuid;
             NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString *myDocsPath    = [myPathList  objectAtIndex:0];
             NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:evimeUuid];
             NSError *error;
             
             NSString *thumbImageFileName = [NSString stringWithFormat:@"th_%@",imageFileName];
             
             NSString *jpgPath = [currentImagesDir stringByAppendingPathComponent:imageFileName];
             NSString *jpgThumbPath = [currentImagesDir stringByAppendingPathComponent:thumbImageFileName];
             
             [[NSFileManager defaultManager] removeItemAtPath:jpgPath error:&error];
             [[NSFileManager defaultManager] removeItemAtPath:jpgThumbPath error:&error];
             
             NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFImage"];
             fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO],nil];
             
             if(imageFileName){
                 NSPredicate *predicate;
                 predicate = [NSPredicate predicateWithFormat:@"imageFile==%@ && evimeUUID=%@",imageFileName, self.currentEvime.uuid];
                 [fetchRequest setPredicate:predicate];
                 EVFImage *image =[self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                 if(image){
                     [self.managedObjectContext deleteObject:image];
                 }
             }
         
         }
         
         
         [self.elementsList removeObjectAtIndex:indexPath.row];
         [self.elementsDataList removeObjectAtIndex:indexPath.row];
         
         for(NSMutableDictionary *contentData in self.elementsDataList){
             [contentData setObject:[NSNumber numberWithInteger:[self.elementsDataList indexOfObject:contentData]] forKey:@"index"];
         }
         
         //NSLog(@"self.pageFullData : %@",self.bodyData );
         self.currentEvime.body = [self.bodyData  JSONRepresentation];
         self.currentEvime.status = @"notuploaded";
         self.currentEvime.lastEditDate = [NSDate date];
         NSError *error;
         [self.managedObjectContext save:&error];
         //[[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
         
         [tableView beginUpdates];
         [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
         [tableView endUpdates];
     
         if(self.isTableScaled){
             
             [self.activeTextView resignFirstResponder];
             
             [UIView animateWithDuration:0.5 animations:^{
                 self.isTableScaled = NO;
                 CGRect tableFrame = self.constructTableView.frame;
                 tableFrame.size.height+=self.tableSqueezeHeight;
                 self.constructTableView.frame = tableFrame;
                 
             }];
             
         }

        
         
     }
     
 }


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    EVFElementView *elementView = [self.elementsList objectAtIndex:indexPath.row];
    
    if(([elementView isEqual:self.activeElementView]) && self.isTextEditingMethod){
        UITextView *textView = (UITextView *)elementView.contentView;
        
        CGSize stringSize;
        NSString *textToMeasure = [textView.text copy];
        
        if(textView.text.length==0){
            
            textToMeasure=@"";
            
        }else{
        
            
            NSString *lastChar = [textToMeasure substringFromIndex:textView.text.length-1];
           
            if([lastChar isEqualToString:@"\n"]){
                
                textToMeasure = [textToMeasure stringByAppendingString:@"-"];
            }
            
        }
        if(self.activeElementView.type ==  kElementTypeText){
            NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:16] forKey: NSFontAttributeName];
            
            stringSize = [textToMeasure boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                                                       options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                                                    attributes:stringAttributes context:nil].size;
            
            
            
        }else{
            
            NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:16] forKey: NSFontAttributeName];
            
            stringSize = [textToMeasure boundingRectWithSize:CGSizeMake(270, MAXFLOAT)
                                                                                       options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                                                                    attributes:stringAttributes context:nil].size;
            
        }
        //NSLog(@"textView.text: %@", textView.text);
        //NSLog(@"stringSize.height+28: %f",stringSize.height+27);
        return stringSize.height+27;
        
    }
    
    //NSLog(@"elementView.frame.size.height: %f, index: %d",elementView.frame.size.height, indexPath.row);
    return elementView.frame.size.height;
}


 // Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    EVFElementView *elementView = [self.elementsList objectAtIndex:fromIndexPath.row];
    [self.elementsList removeObjectAtIndex:fromIndexPath.row];
    [self.elementsList insertObject:elementView atIndex:toIndexPath.row];
    [self.constructTableView reloadData];
    
    NSMutableDictionary *contentDataMoved = [self.elementsDataList objectAtIndex:fromIndexPath.row];
    
    [self.elementsDataList removeObjectAtIndex:fromIndexPath.row];
    [self.elementsDataList insertObject:contentDataMoved atIndex:toIndexPath.row];
    
    
    for(NSMutableDictionary *contentData in self.elementsDataList){
        [contentData setObject:[NSNumber numberWithInteger:[self.elementsDataList indexOfObject:contentData]] forKey:@"index"];
    }
    
    //NSLog(@"self.pageFullData : %@",self.bodyData );
    
    self.currentEvime.body = [self.bodyData  JSONRepresentation];
    self.currentEvime.status = @"notuploaded";
    self.currentEvime.lastEditDate = [NSDate date];
    NSError *error;
    [self.managedObjectContext save:&error];
    //[[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
     
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
 // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImageView *imageContentView = (UIImageView *)self.activeElementView.contentView;
   
    
    UIImage *newImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage *thumbImage;
    UIImage *fullImage;
    fullImage = [newImage imageScaleAspectToMaxSize:1280];
    
    if(newImage.size.height>newImage.size.width){
        
        thumbImage = [newImage imageScaleAspectToMaxSize:560];
        
        float aspectRatio = thumbImage.size.height/thumbImage.size.width;
        
        CGRect frame = imageContentView.frame;
        frame.size.width = 280.0/aspectRatio;
        frame.size.height = 280.0;
        
        //NSLog(@"frame.size.w.h: %f, %f", frame.size.width, frame.size.height);
        frame.origin.x = (280.0-frame.size.width)/2.0;
        imageContentView.frame = frame;
        
        CGRect superFrame = self.activeElementView.frame;
        superFrame.size.height = 290.0;
        
        self.activeElementView.aspectRatio = aspectRatio;
        self.activeElementView.frame = superFrame;
        
        
    }else{
        
        thumbImage = [newImage imageScaleAspectToMaxSize:440];
        
        float aspectRatio = thumbImage.size.width/thumbImage.size.height;
        
        CGRect frame = imageContentView.frame;
        frame.size.width = 280.0;
        frame.size.height = 280.0/aspectRatio;
        frame.origin.x = 0.0;
        imageContentView.frame = frame;
        
        //NSLog(@"frame.size.w.h: %f, %f", frame.size.width, frame.size.height);
        
        CGRect superFrame = self.activeElementView.frame;
        superFrame.size.height = frame.size.height + 10.0;
        
        self.activeElementView.aspectRatio = aspectRatio;
        self.activeElementView.frame = superFrame;
        
       
    }
    
    [self.constructTableView beginUpdates];
    [self.constructTableView endUpdates];
    
    
    NSString *evimeUUID = self.currentEvime.uuid;
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *myDocsPath    = [myPathList  objectAtIndex:0];
    NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:evimeUUID];
    NSError *error;
    BOOL isDir;
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:currentImagesDir isDirectory:&isDir])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:currentImagesDir withIntermediateDirectories:YES attributes:nil error:&error];
           
    }
    
    NSString *UUID = [[NSUUID UUID] UUIDString];
    
    NSString *thumbImageFileName = [NSString stringWithFormat:@"th_%@.jpg",UUID];
    
    NSString *jpgPath = [currentImagesDir stringByAppendingPathComponent:thumbImageFileName];
    [UIImageJPEGRepresentation(thumbImage, 0.8) writeToFile:jpgPath atomically:YES];
    
    imageContentView.image = thumbImage;
    
    
    NSString *fullImageFileName = [NSString stringWithFormat:@"%@.jpg",UUID];
    
    jpgPath = [currentImagesDir stringByAppendingPathComponent:fullImageFileName];
    [UIImageJPEGRepresentation(fullImage, 0.8) writeToFile:jpgPath atomically:YES];
    
    
    
    [self.activeContentData setObject:fullImageFileName forKey:@"content"];
    [self.activeContentData setObject:[NSNumber numberWithFloat:self.activeElementView.frame.size.height] forKey:@"height"];
    
    //NSLog(@"self.pageFullData : %@",self.bodyData );
    
    self.currentEvime.body = [self.bodyData  JSONRepresentation];
    
    EVFImage *image = [NSEntityDescription insertNewObjectForEntityForName:@"EVFImage" inManagedObjectContext:self.managedObjectContext];
    image.evimeUUID = evimeUUID;
    image.imageFile = fullImageFileName;
    image.status = @"notuploaded";
    self.currentEvime.status = @"notuploaded";
    self.currentEvime.lastEditDate = [NSDate date];
    [self.managedObjectContext save:&error];
    self.isNewImage = NO;
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
    }else{
        [[[EVFLoadingProcessing alloc] init] uploadImages];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    if(self.isNewImage){
    
        [self.elementsList removeLastObject];
        [self.elementsDataList removeLastObject];
        
        [self.constructTableView reloadData];
        //NSLog(@"self.pageFullData : %@",self.bodyData );
        
        self.currentEvime.body = [self.bodyData  JSONRepresentation];
        
        NSError *error;
        [self.managedObjectContext save:&error];
        self.isNewImage = NO;
    }
    
}



@end
