//
//  EVFLoginRequest.m
//  EVIME
//
//  Created by Evgeny on 15/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFLoginRequest.h"
#import "EVFCryptoHelper.h"
#import "EVFAccount.h"
#import "SBJson.h"
#import "EVFProperty.h"
#import "EVFAppDelegate.h"
#import "EVFLogProcessing.h"
#import "EVFLogNote.h"

@interface EVFLoginRequest()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@implementation EVFLoginRequest

-(void)sendRequestForAccount:(EVFAccount *)account {

    NSString *passKey = [EVFCryptoHelper passKeyForUsername:account.username andPassword:account.password ];

    //NSLog(@"passKey:%@",passKey);

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/login.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];

    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    
    NSString *deviceToken = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceToken];
    NSString *deviceUUID =  [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    [requestData setObject:account.username forKey:@"username"];
    [requestData setObject:passKey forKey:@"passKey"];
    if(deviceToken){
        [requestData setObject:deviceToken forKey:@"deviceToken"];
    }else{
        [requestData setObject:@"" forKey:@"deviceToken"];
    }
    [requestData setObject:deviceUUID forKey:@"deviceUUID"];
    
    NSString *requestJson = [requestData JSONRepresentation];
    
    NSString *bodyEncodedString = [[requestJson dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString* str = [NSString stringWithFormat:@"data=%@",bodyEncodedString];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection failed!" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                   [alertView show];
                                   //NSLog(@"request error: %@", error.localizedDescription);
                                   [self.delegate loginDidFail];
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"responseDict: %@", responseDict);
                                   
                                   if(responseDict){
                                       
                                       if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                           
                                           self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
                                           
                                           
                                           NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFProperty"];
                                           fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"key" ascending:NO],nil];
                                           NSPredicate *predicate;
                                           predicate = [NSPredicate predicateWithFormat:@"key=='Username'"];
                                           [fetchRequest setPredicate:predicate];
                                           
                                           EVFProperty *usernameProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                           if(!usernameProperty){
                                               usernameProperty = [NSEntityDescription insertNewObjectForEntityForName:@"EVFProperty" inManagedObjectContext:self.managedObjectContext];

                                           }
                                           
                                           predicate = [NSPredicate predicateWithFormat:@"key=='FullName'"];
                                           [fetchRequest setPredicate:predicate];
                                           
                                           EVFProperty *fullNameProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
                                           if(!fullNameProperty){
                                               fullNameProperty = [NSEntityDescription insertNewObjectForEntityForName:@"EVFProperty" inManagedObjectContext:self.managedObjectContext];
                                           }
                                           
                                           usernameProperty.key = @"Username";
                                           usernameProperty.value = account.username;
                                           
                                           NSString *fullName = [responseDict objectForKey:@"FullName"];
                                           fullNameProperty.key = @"FullName";
                                           fullNameProperty.value = fullName;
                                           fullNameProperty.status = @"downloaded";
                                           
                                           EVFProperty *isAuthenticated = [NSEntityDescription insertNewObjectForEntityForName:@"EVFProperty" inManagedObjectContext:self.managedObjectContext];
                                           isAuthenticated.key = @"IsAuthenticated";
                                           isAuthenticated.value = @"YES";
                                           
                                           [self.managedObjectContext save:&error];
                                          
                                           NSString *sessionKey = [responseDict objectForKey:@"SessionKey"];
                                           
                                           [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setSessionKey:sessionKey];
                                           [EVFCryptoHelper saveSessionKey:sessionKey];
                                           
                                           [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setUsername:usernameProperty.value];
                                           [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setUserFullName:fullNameProperty.value];
                                           
                                           [self.delegate loginDidFinish];
                                           
                                           
                                       }else if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:-2]]){
                                           
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid username/password." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate loginDidFail];
                                           
                                       }else{
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unexpected error!" message:@"Please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate loginDidFail];
                                           
                                           EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                           logNote.eventId = 8;
                                           logNote.note = [NSString stringWithFormat:@"LoginRequest:%@_____Response %@",str,responseString];
                                           logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                           
                                           [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                       }
                                       
                                       
                                   }else{
                                       
                                       UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unexpected error!" message:@"Please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                       [alertView show];
                                       
                                       [self.delegate loginDidFail];
                                      
                                           
                                       EVFLogNote *logNote = [[EVFLogNote alloc] init];
                                       logNote.eventId = 8;
                                       logNote.note = [NSString stringWithFormat:@"LoginRequest:%@_____Response %@",str,responseString];
                                       logNote.username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
                                       
                                       [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
                                           
                                       

                                       
                                   }
                                   
                                   
                               }
                               
                               
                           }];
}

@end
