//
//  EVFLogNote.h
//  EVIME
//
//  Created by Evgeny on 17/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EVFLogNote : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *note;
@property (nonatomic) NSInteger eventId;

@end
