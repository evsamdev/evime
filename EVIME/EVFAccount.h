//
//  EVFAccount.h
//  EVIME
//
//  Created by Evgeny on 1/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EVFAccount : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *confirmPassword;
@property (copy, nonatomic) NSString *notValidMessage;
-(BOOL)isAccountDataValidForCreate;
-(BOOL)isAccountDataValidForLogin;
-(BOOL)isAccountDataValidForResetPassword;
@end
