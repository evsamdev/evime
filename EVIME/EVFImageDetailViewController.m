//
//  EVFImageDetailViewController.m
//  EVIME
//
//  Created by Evgeny on 31/10/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFImageDetailViewController.h"
#define kIsWideScreen ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
@interface EVFImageDetailViewController ()

@property (nonatomic, strong) UIToolbar *toolBar;
@property (nonatomic, strong) NSMutableDictionary *filters;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic) CGFloat lastScale;
@property (nonatomic) BOOL statusBarHidden;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTapGestureRecognizer;
@property (nonatomic, strong) UIPinchGestureRecognizer *pinchGestureRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic) CGFloat beginX;
@property (nonatomic) CGFloat beginY;
@end

@implementation EVFImageDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotate {
    
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
        
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.toolBar = [[UIToolbar alloc] init];
    
    self.fullscreenImageView.image = self.currentImage;
    
    float imageHeight;
    float imageWidth;
    float imageX;
    float imageY;
    
    float aspectRatio = self.currentImage.size.height/self.currentImage.size.width;
    
    imageWidth = 320;
    imageHeight = imageWidth*aspectRatio;
    imageX = 0;
    imageY = (self.view.frame.size.height-imageHeight)/2;
    
    //NSLog(@"imageX: %f, imageWidth: %f, imageHeight: %f, imageY: %f,self.view.frame.size.height: %f", imageX, imageWidth, imageHeight,imageY,self.view.frame.size.height);
    
    if(imageHeight>self.view.frame.size.height-64){
        
        imageY = 32;
        imageHeight=self.view.frame.size.height-64;
        imageWidth = imageHeight/aspectRatio;
        imageX =(320-imageWidth)/2.0;
     
    }
    
    self.imageScrollView.frame = CGRectMake(0,0,320, kIsWideScreen?568:480);
    self.fullscreenImageView.frame = CGRectMake(imageX, imageY, imageWidth, imageHeight);
    //self.imageScrollView.contentSize = self.currentImage.size;
    self.lastScale = 1.0;
    self.fullscreenImageView.userInteractionEnabled = YES;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL) prefersStatusBarHidden {
    return self.statusBarHidden;
    
}

- (void) _updateBars:(NSTimeInterval)animationDuration {
    
    BOOL hidden = !self.toolBar.hidden; // Flip our hidden state
    
    if (hidden && self.navigationController.topViewController != self) {
        //NSLog(@"%s: Asked to hide bar, but not the top view controller, skipping update of bars", __PRETTY_FUNCTION__);
        return;
    }
    
    // Animate the alpha for navbar and toolbar, and animate status bar's hidden state
    void (^animationBlock)() = ^{
        CGFloat alpha = hidden ? 0.0 : 1.0;
        [[UIApplication sharedApplication] setStatusBarHidden:hidden];
        self.statusBarHidden = hidden;
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            [self setNeedsStatusBarAppearanceUpdate];
        }
        if (hidden == NO) {
            self.toolBar.hidden = NO;
            [self.navigationController setNavigationBarHidden:NO animated:NO];
        }
        self.toolBar.alpha =  alpha;
        self.navigationController.navigationBar.alpha = alpha;
    };
    
    // If we're hiding, after animation completes really make navbar and statusbar hidden
    void (^completionBlock)(BOOL finished) = ^(BOOL finished) {
        if (finished && hidden) {
            self.toolBar.hidden = YES;
            [self.navigationController setNavigationBarHidden:YES animated:NO];
        }
    };
    
    if (animationDuration > 0.0) {
        [UIView animateWithDuration:animationDuration animations:animationBlock completion:completionBlock];
    }
    else {
        animationBlock();
        completionBlock(YES);
    }
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    // If going to landscape
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        
        float imageHeight;
        float imageWidth;
        float imageX;
        float imageY;
        
        float aspectRatio = self.currentImage.size.width/self.currentImage.size.height;
        
        imageHeight = 320;
        imageWidth = imageHeight*aspectRatio;
        
        imageX = (self.view.frame.size.width-imageWidth)/2;
        imageY = 0;
                
        if(imageWidth>self.view.frame.size.height){
            imageX = 0;
            imageWidth = self.view.frame.size.height;
            imageHeight=imageWidth/aspectRatio;
            imageY =(320-imageHeight)/2.0;
        }
        
        //NSLog(@"imageX: %f, imageWidth: %f, imageHeight: %f, imageY: %f", imageX, imageWidth, imageHeight,imageY);
        
        if(self.imageScrollView.zoomScale == 1.0){
            self.imageScrollView.contentSize = CGSizeMake(imageWidth,imageHeight);
            self.fullscreenImageView.bounds = CGRectMake(0, 0, imageWidth, imageHeight);
        }else{
            
            
        }
        
        
        //self.fullscreenImageView.center = self.imageScrollView.center;
    }
    // If going to portrait
    else {
        
        float imageHeight;
        float imageWidth;
        float imageX;
        float imageY;
        
        float aspectRatio = self.currentImage.size.height/self.currentImage.size.width;
        
        imageWidth = 320;
        imageHeight = imageWidth*aspectRatio;
        imageX = 0;
        imageY = (self.view.frame.size.height-imageHeight)/2;
        
        if(imageHeight>self.view.frame.size.height-64){
            imageY = 0;
            imageHeight=self.view.frame.size.width-64;
            imageWidth = imageHeight/aspectRatio;
            imageX =(320-imageWidth)/2.0;
            
            if(imageX<0){
                float aspectRatioAlt = self.currentImage.size.width/self.currentImage.size.height;
                imageX = 0;
                imageWidth=320;
                imageHeight = imageWidth/aspectRatioAlt;
                imageY =(320-imageHeight)/2.0;
                
            }
                        
        }
        
        if(self.imageScrollView.zoomScale == 1.0){
            self.imageScrollView.contentSize = CGSizeMake(imageWidth,imageHeight);
            self.fullscreenImageView.bounds = CGRectMake(0, 0, imageWidth, imageHeight);
            
        }else{
            
        }
        
        
        //NSLog(@"imageX: %f, imageWidth: %f, imageHeight: %f, imageY: %f", imageX, imageWidth, imageHeight,imageY);
       
        
        //self.fullscreenImageView.center = self.imageScrollView.center;
    }
    
    // If going to landscape, hide status bar if showing
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        if (self.statusBarHidden == NO) {
            [self _updateBars:0.0];
        }
    }
    // If going to portrait, show status bar if hidden
    else {
        if (self.statusBarHidden == YES) {
            [self _updateBars:0.0];
        }
    }
}

- (void) handleSingleTapGesture:(UIGestureRecognizer *)gestureRecognizer {
    [self _updateBars:0.25];
}

- (void) handleDoubleTapGesture:(UIGestureRecognizer *)gestureRecognizer {
   
    //NSLog(@"handleDoubleTapGesture");
    //CGPoint pointInView = [gestureRecognizer locationInView:self.fullscreenImageView];
    
    if(self.imageScrollView.zoomScale == 1.0){
        [self.imageScrollView setZoomScale:2.01 animated:YES];
        
    }else{
        [self.imageScrollView setZoomScale:1.0 animated:YES];
        
    }
    
}


- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (self.statusBarHidden == NO) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                // Make sure we're still in landscape, and the status bar isn't already hidden
                if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && (self.statusBarHidden == NO)) {
                    [self _updateBars:0.75];
                }
            });
        }
    }
    
    if (self.tapGestureRecognizer == nil) {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
        [self.fullscreenImageView addGestureRecognizer:self.tapGestureRecognizer];
    }
    if (self.doubleTapGestureRecognizer == nil) {
        self.doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapGesture:)];
        self.doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        [self.fullscreenImageView addGestureRecognizer:self.doubleTapGestureRecognizer];
    }
   
    [self.tapGestureRecognizer requireGestureRecognizerToFail:self.doubleTapGestureRecognizer];
    
    
    self.imageScrollView.minimumZoomScale = 1.0;
    self.imageScrollView.maximumZoomScale = 10.0f;
    self.imageScrollView.zoomScale = 1.0;
    
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return self.fullscreenImageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
}

- (void)centerScrollViewContents {
    
    //NSLog(@"centerScrollViewContents: %f",self.imageScrollView.zoomScale);
    
    CGSize boundsSize = self.imageScrollView.bounds.size;
    CGRect contentsFrame = self.fullscreenImageView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.fullscreenImageView.frame = contentsFrame;
}


- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.toolBar.hidden) {
        [self _updateBars:0.0];
    }
}

- (void) _cleanupGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    [gestureRecognizer removeTarget:nil action:NULL];
    [self.fullscreenImageView removeGestureRecognizer:gestureRecognizer];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self _cleanupGestureRecognizer:self.tapGestureRecognizer]; self.tapGestureRecognizer = nil;
    [self _cleanupGestureRecognizer:self.doubleTapGestureRecognizer]; self.doubleTapGestureRecognizer = nil;
   
}

- (IBAction)share:(id)sender {
    if (self.fullscreenImageView.image) {
        UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObject:self.fullscreenImageView.image] applicationActivities:nil];
        avc.restorationIdentifier = @"Activity";
        [self presentViewController:avc animated:YES completion:nil];
    }
}

@end
