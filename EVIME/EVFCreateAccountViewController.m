//
//  EVFCreateAccountViewController.m
//  EVIME
//
//  Created by Evgeny on 1/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFCreateAccountViewController.h"
#import "EVFAccount.h"
#import "EVFCreateAccountRequest.h"


@interface EVFCreateAccountViewController ()<EVFCreateAccountRequestDelegate>

@property (nonatomic,strong) EVFCreateAccountRequest *createAccountRequest;
@property (nonatomic) BOOL isAgree;
@end

@implementation EVFCreateAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	   
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    UIToolbar *overToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    overToolbar.barStyle = UIBarStyleDefault;
    overToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:@"Hide" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissKeyboard)],
                         nil];
    
    self.usernameTextField.inputAccessoryView = overToolbar;
    self.emailTextField.inputAccessoryView = overToolbar;
    self.fullNameTextField.inputAccessoryView = overToolbar;
    self.passwordTextField.inputAccessoryView = overToolbar;
    self.confirmPasswordTextField.inputAccessoryView = overToolbar;
    self.isAgree = NO;
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(void)dismissKeyboard {
    
    [self.usernameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
    [self.fullNameTextField resignFirstResponder];
    [UIView beginAnimations:@"Animate Text Field Up" context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.view.frame = CGRectMake(self.view.frame.origin.x,
                                 64,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height);
    
    [UIView commitAnimations];
}


- (IBAction)submit:(UIButton *)sender {
    [self dismissKeyboard];
    
    EVFAccount *account = [[EVFAccount alloc] init];
    
    account.username = self.usernameTextField.text;
    account.email = self.emailTextField.text;
    account.password = self.passwordTextField.text;
    account.confirmPassword = self.confirmPasswordTextField.text;
    account.fullName = self.fullNameTextField.text;
    
    if(![account isAccountDataValidForCreate]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:account.notValidMessage message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    /*
    if(!self.isAgree){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"You need to Agree TaC and PP!" message:@"To register you need to Agree" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        return;
    
    }
    */
    
    self.createAccountRequest = [[EVFCreateAccountRequest alloc] init];
    self.createAccountRequest.delegate = self;
    [self.createAccountRequest sendRequestForAccount:account];
    [self.activityIndicator startAnimating];
    self.view.userInteractionEnabled=NO;
    
}

- (IBAction)cancel:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    //NSLog(@"textField.tag: %d",textField.tag);
    
    if(textField.tag == 22){
        
        [UIView beginAnimations:@"Animate Text Field Up" context:nil];
        [UIView setAnimationDuration:.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        
        
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     -10,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
        
        
        [UIView commitAnimations];
        
        
    }

    
    
    if(textField.tag == 23){
        
        [UIView beginAnimations:@"Animate Text Field Up" context:nil];
        [UIView setAnimationDuration:.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        
        
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     -40,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
        
        
        [UIView commitAnimations];
        
        
    }

    
    
    
    if(textField.tag > 23){
        
        [UIView beginAnimations:@"Animate Text Field Up" context:nil];
        [UIView setAnimationDuration:.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        
        if([self hasFourInchDisplay]){
            self.view.frame = CGRectMake(self.view.frame.origin.x,
                                         self.view.frame.size.height-480-114,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height);
        }else{
            self.view.frame = CGRectMake(self.view.frame.origin.x,
                                         self.view.frame.size.height-480-94,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height);

        
        }
        
        [UIView commitAnimations];
        
        
    }
    
    return YES;
}

- (BOOL)hasFourInchDisplay {
    return ([UIScreen mainScreen].bounds.size.height == 568.0);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if(textField.tag==25){
        [UIView beginAnimations:@"Animate Text Field Up" context:nil];
        [UIView setAnimationDuration:.2];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     64,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
        [UIView commitAnimations];
        [self dismissKeyboard];
        //[self submit:nil];
    }else{
        UITextField *nextTextField = ( UITextField *)[self.view viewWithTag:textField.tag+1];
        [nextTextField becomeFirstResponder];
    }
    
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    NSString *text = [textField.text stringByAppendingString:string];
    
    if(textField.tag ==21){
    
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_-"] invertedSet];
        
        if ([string rangeOfCharacterFromSet:set].location != NSNotFound || text.length>20) {
            //NSLog(@"This string contains illegal characters");
            return NO;
        }
        
    }
    
    if(textField.tag == 22){
        
        if(text.length>36){
            return NO;
        }
    
    }
    
    if((textField.tag ==24 || textField.tag ==25) && (([string isEqualToString:@"'"] || [string isEqualToString:@"\""] || [string isEqualToString:@" "] || [string isEqualToString:@"\\"]) || text.length>20)){
        return NO;
    }
        
    
    return YES;
}



#pragma mark CreateAccountRequestDelegate

-(void)createAccountDidFinish{
    self.view.userInteractionEnabled=YES;
    [self.activityIndicator stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"You have successfully registered." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
    
}

-(void)createAccountDidFail{
    self.view.userInteractionEnabled=YES;
    [self.activityIndicator stopAnimating];
    
    
}

#pragma mark -
#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.title isEqualToString:@"Incorrect data!"]){
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)toggleAgreeCheckbox:(UIButton *)sender {
    
    self.isAgree = !self.isAgree;
    
    if(self.isAgree){
        [self.agreeCheckboxButton setBackgroundImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateNormal];
        [self.agreeCheckboxButton setBackgroundImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateHighlighted];
    }else{
        [self.agreeCheckboxButton setBackgroundImage:[UIImage imageNamed:@"checkboxOff.png"] forState:UIControlStateNormal];
        [self.agreeCheckboxButton setBackgroundImage:[UIImage imageNamed:@"checkboxOn.png"] forState:UIControlStateHighlighted];
    }
    
    
}

- (IBAction)showTerms:(UIButton *)sender{
    
   
    NSURL *url  = [NSURL URLWithString:@"http://www.enotekit.com/terms.html"];
    [[UIApplication sharedApplication] openURL:url];
    
    
}

- (IBAction)showPrivacy:(UIButton *)sender{

    NSURL *url  = [NSURL URLWithString:@"http://www.enotekit.com/privacy.html"];
    [[UIApplication sharedApplication] openURL:url];
    
}

@end
