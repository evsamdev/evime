//
//  EVFEmailGenerator.m
//  EVIME
//
//  Created by Evgeny on 3/05/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFEmailGenerator.h"
#import "SBJson.h"
#import "EVFElementView.h"

@implementation EVFEmailGenerator

const NSString *imageTag=@"\
<tr>\
<td align='center' class='hero'>\
<a href='https://enotekit.com/mobile_app/api/uploads/{__##__}'><img src='https://enotekit.com/mobile_app/api/uploads/{__##__}'  alt='' style='display: block; border: 0; width: 260px;' /></a><br>\
</td>\
</tr>";

const NSString *headerTag = @"\
<tr>\
<td align='center'  style='font-family: arial,sans-serif; color: #333; font-size: 16px; font-weight:bold'>\
{__##__}<br><br>\
</td>\
</tr>";

const NSString *textTag=@"\
<tr>\
<td align='left' style='font-family: arial,sans-serif; font-size: 16px; line-height: 20px !important; color: #333; padding-bottom: 20px;'>\
{__##__}\
</td>\
</tr>\
<tr>";


-(NSString *)htmlEmailBodyForEvime:(EVFEvime *)evime{
    
    NSString *templatePath = [[NSBundle mainBundle] pathForResource:@"email_template" ofType:@"html"];
    
    NSString *template = [NSString stringWithContentsOfFile:templatePath encoding:NSUTF8StringEncoding error:nil];
    
    NSString *sourceHtml=@"";
    
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    
    NSDictionary *evimeBodyData = [jsonParser objectWithString:evime.body];
    
    NSDictionary *mainPageData = [evimeBodyData objectForKey:@"page"];
    NSArray *elmentsDataList =  [mainPageData objectForKey:@"elements"];
    //NSLog(@"elmentsDataList: %@",elmentsDataList);
    
    if(elmentsDataList){
        NSString *elementsHtml=[self processElementsDataList:elmentsDataList html:sourceHtml];
    
        NSString *resultHtml = [template stringByReplacingOccurrencesOfString:@"{__##__}" withString:elementsHtml];
    
        return resultHtml;
    }
    else return @"";
}


-(NSString *)processElementsDataList:(NSArray *)elmentsDataList html:(NSString *)sourceHtml{
    
    NSString *resultHtml=@"";
    
    for(NSDictionary *elementData in elmentsDataList){
        
        
        NSString *elementHtml=@"";
        
        NSInteger type = [[elementData objectForKey:@"type"] integerValue];
        
        switch (type) {
            case kElementTypeButton:
            case kElementTypeHeader:{
                NSString *headerText = [elementData objectForKey:@"content"];
                if(headerText){
                    elementHtml = [headerTag stringByReplacingOccurrencesOfString:@"{__##__}" withString:headerText];
                    elementHtml = [elementHtml stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
                    
                }
                break;
            }
            case kElementTypeImage:{
                NSString *imageFileName = [elementData objectForKey:@"content"];
                if(imageFileName){
                    
                    NSString *dir = [imageFileName substringToIndex:3];
                    NSString *file = [imageFileName substringFromIndex:3];
                    NSString *imageServerFileName = [NSString stringWithFormat:@"%@/%@",dir,file];
                    
                    
                    elementHtml = [imageTag stringByReplacingOccurrencesOfString:@"{__##__}" withString:imageServerFileName];
                   
                }
                break;
            }
            case kElementTypeText:{
                NSString *textText = [elementData objectForKey:@"content"];
                if(textText){
                    elementHtml = [textTag stringByReplacingOccurrencesOfString:@"{__##__}" withString:textText];
                    elementHtml = [elementHtml stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
                }
                break;
            }

            default:
                break;
                
        }
        resultHtml = [resultHtml stringByAppendingString:elementHtml];
        
        NSDictionary *childPageData = [elementData objectForKey:@"page"];
        NSArray *elmentsDataList =  [childPageData objectForKey:@"elements"];
        
        NSString *resultSubHtml = [self processElementsDataList:elmentsDataList html:sourceHtml];
        resultHtml = [resultHtml stringByAppendingString:resultSubHtml];
    }
    
    
    
    return resultHtml;
}




@end
