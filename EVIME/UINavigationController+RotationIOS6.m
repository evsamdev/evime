//
//  UINavigationController+RotationIOS6.m
//  CheckPoint
//
//  Created by Evgeny on 03.09.13.
//  Copyright (c) 2013 vittoria. All rights reserved.
//

#import "UINavigationController+RotationIOS6.h"

@implementation UINavigationController (RotationIOS6)

-(BOOL)shouldAutorotate
{
    return [self.topViewController shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}

@end
