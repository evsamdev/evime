//
//  EVFLoginRequest.h
//  EVIME
//
//  Created by Evgeny on 15/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EVFAccount;
@protocol EVFLoginRequestDelegate <NSObject>
-(void) loginDidFinish;
-(void) loginDidFail;
@end

@interface EVFLoginRequest : NSObject

@property (weak, nonatomic) id<EVFLoginRequestDelegate> delegate;
-(void)sendRequestForAccount:(EVFAccount *)account;

@end
