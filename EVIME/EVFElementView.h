//
//  EVFElementView.h
//  EVIME
//
//  Created by Evgeny on 12.10.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kResizeControlSide 40

typedef enum ElementType : NSUInteger {
    kElementTypeHeader,
    kElementTypeImage,
    kElementTypeText,
    kElementTypeButton
} ElementType;


@class EVFConstructViewController;
@interface EVFElementView : UIView

@property (nonatomic) ElementType type;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *controlView;
@property (nonatomic, strong) EVFConstructViewController *childConstructViewcontroller;
@property (nonatomic) CGFloat aspectRatio;

@end
