//
//  UIImagePickerController+UIImagePickerController_NonRotating.m
//  EVIME
//
//  Created by Evgeny on 12/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "UIImagePickerController+UIImagePickerController_NonRotating.h"

@implementation UIImagePickerController (UIImagePickerController_NonRotating)


- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

@end
