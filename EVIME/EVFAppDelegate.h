//
//  EVFAppDelegate.h
//  EVIME
//
//  Created by Evgeny on 28.09.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EVFEvime.h"

@interface EVFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) EVFEvime *evimeForEdit;
@property (nonatomic, copy) NSString *deviceToken;
@property (nonatomic, copy) NSString *deviceUUID;
@property (nonatomic, copy) NSString *sessionKey;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *userFullName;
@property (nonatomic, copy) NSString *destinationBox;
@property (nonatomic) BOOL isAuthenticated;
@property (nonatomic) BOOL isLogoutAction;
@property (nonatomic) BOOL isLoginAction;
@end
