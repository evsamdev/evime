//
//  EVFContactsViewController.h
//  EVIME
//
//  Created by Evgeny on 27/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFContactsViewController : UIViewController

- (IBAction)createNewContact:(UIBarButtonItem *)sender;

- (IBAction)inviteUser:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;

- (void)myInit;

@end
