//
//  EVFProperty.h
//  EVIME
//
//  Created by Evgeny on 29/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EVFProperty : NSManagedObject

@property (nonatomic, retain) NSString * key;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSString * status;

@end
