//
//  EVFLoginViewController.m
//  EVIME
//
//  Created by Evgeny on 1/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFLoginViewController.h"
#import "EVFAccount.h"
#import "EVFLoginRequest.h"
#import "EVFLoadingProcessing.h"
#import "EVFProperty.h"
#import "EVFAppDelegate.h"
#import "EVFResetPasswordRequest.h"
#import "EVFViewController.h"
#import "EVFEvime.h"
#import "EVFContact.h"

@interface EVFLoginViewController ()<EVFLoginRequestDelegate, EVFResetPasswordRequestDelegate>
@property (nonatomic,strong) EVFLoginRequest *loginRequest;
@property (nonatomic,strong) EVFResetPasswordRequest *resetPasswordRequest;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
- (IBAction)cancel:(UIBarButtonItem *)sender;

@end

@implementation EVFLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    UIToolbar *overToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    overToolbar.barStyle = UIBarStyleDefault;
    overToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:@"Hide" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissKeyboard)],
                         nil];
    
    self.usernameTextField.inputAccessoryView = overToolbar;
    self.passwordTextField.inputAccessoryView = overToolbar;
    
    NSManagedObjectContext *managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFProperty"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"key" ascending:NO],nil];
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"key=='Username'"];
    [fetchRequest setPredicate:predicate];
    EVFProperty *usernameProperty = [managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
    if(usernameProperty){
        self.usernameTextField.text = usernameProperty.value;
    }
    
       
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(void)dismissKeyboard {
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(UIButton *)sender {
    [self dismissKeyboard];
    EVFAccount *account = [[EVFAccount alloc] init];

    account.username = self.usernameTextField.text;
    account.password = self.passwordTextField.text;
    
    if(![account isAccountDataValidForLogin]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:account.notValidMessage message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    self.loginRequest = [[EVFLoginRequest alloc] init];
    self.loginRequest.delegate = self;
    [self.loginRequest sendRequestForAccount:account];
    [self.activityIndicator startAnimating];
    self.view.userInteractionEnabled=NO;
    
    
}

- (IBAction)forgotPassword:(UIButton *)sender {
    
    [self dismissKeyboard];
    EVFAccount *account = [[EVFAccount alloc] init];
    
    account.username = self.usernameTextField.text;
    
    if(![account isAccountDataValidForResetPassword]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:account.notValidMessage message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    self.resetPasswordRequest = [[EVFResetPasswordRequest alloc] init];
    self.resetPasswordRequest.delegate = self;
    [self.resetPasswordRequest sendRequestForAccount:account];
    [self.activityIndicator startAnimating];
    self.view.userInteractionEnabled=NO;
    
    
}

#pragma mark LoginRequestDelegate

-(void)loginDidFinish{
    [self.activityIndicator stopAnimating];
    self.view.userInteractionEnabled=YES;
    //NSLog(@"loginDidFinish");
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
    
    NSPredicate *predicate;
    NSString *username;
   
    username = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username];
    
    NSManagedObjectContext *managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    predicate = [NSPredicate predicateWithFormat:@"user == '_#_AnonimoussuominonA_#_'"];
    [fetchRequest setPredicate:predicate];
    
    NSArray *evimesList=[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for(EVFEvime *evime in evimesList){
        evime.user = username;
    }
    
    fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFContact"];
    predicate = [NSPredicate predicateWithFormat:@"user == '_#_AnonimoussuominonA_#_'"];
    [fetchRequest setPredicate:predicate];
    
    NSArray *contactList=[managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for(EVFContact *contact in contactList){
        contact.user = username;
    }
    
    [managedObjectContext save:&error];
    
    [[[EVFLoadingProcessing alloc] init] downloadCurrentEvimes];
    
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsAuthenticated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}



-(void)loginDidFail{
    [self.activityIndicator stopAnimating];
    //NSLog(@"loginDidFail");
    self.view.userInteractionEnabled=YES;

}

#pragma mark ResetPasswordRequestDelegate


-(void)resetPasswordDidFinish{
    [self.activityIndicator stopAnimating];
    self.view.userInteractionEnabled=YES;
    //NSLog(@"loginDidFinish");
}

-(void)resetPasswordDidFail{
    [self.activityIndicator stopAnimating];
     self.view.userInteractionEnabled=YES;
     //NSLog(@"loginDidFail");
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
        
    if(textField.tag==22){
        [self dismissKeyboard];
        [self login:nil];
    }else{
        UITextField *nextTextField = ( UITextField *)[self.view viewWithTag:textField.tag+1];
        [nextTextField becomeFirstResponder];
    }
    
    
    return YES;
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
