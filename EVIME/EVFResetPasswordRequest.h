//
//  EVFResetPasswordRequest.h
//  EVIME
//
//  Created by Evgeny on 5/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EVFAccount;

@protocol EVFResetPasswordRequestDelegate <NSObject>
-(void) resetPasswordDidFinish;
-(void) resetPasswordDidFail;
@end
@interface EVFResetPasswordRequest : NSObject

@property (nonatomic, weak) id<EVFResetPasswordRequestDelegate> delegate;

-(void)sendRequestForAccount:(EVFAccount *)account;

@end
