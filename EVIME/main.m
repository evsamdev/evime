//
//  main.m
//  EVIME
//
//  Created by Evgeny on 28.09.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EVFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EVFAppDelegate class]));
    }
}
