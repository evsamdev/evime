//
//  EVFEvime.h
//  EVIME
//
//  Created by Evgeny on 25/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EVFEvime : NSManagedObject

@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * box;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSDate * deleteDate;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSDate * lastEditDate;
@property (nonatomic, retain) NSNumber * readyness;
@property (nonatomic, retain) NSDate * receiveDate;
@property (nonatomic, retain) NSDate * sendDate;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * trash;
@property (nonatomic, retain) NSString * user;
@property (nonatomic, retain) NSString * userFrom;
@property (nonatomic, retain) NSString * userFullFrom;
@property (nonatomic, retain) NSString * userFullTo;
@property (nonatomic, retain) NSString * userTo;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * sourceUUID;

@end
