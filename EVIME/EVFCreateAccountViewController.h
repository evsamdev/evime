//
//  EVFCreateAccountViewController.h
//  EVIME
//
//  Created by Evgeny on 1/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFCreateAccountViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;


@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;


@property (weak, nonatomic) IBOutlet UITextField *emailTextField;


@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;


@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;


@property (weak, nonatomic) IBOutlet UIButton *agreeCheckboxButton;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)submit:(UIBarButtonItem *)sender;

- (IBAction)toggleAgreeCheckbox:(UIButton *)sender;

- (IBAction)showTerms:(UIButton *)sender;
- (IBAction)showPrivacy:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
