//
//  EVFEvimesListTableViewCell.m
//  EVIME
//
//  Created by Evgeny on 12/05/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFEvimesListTableViewCell.h"

@implementation EVFEvimesListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
