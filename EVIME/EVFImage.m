//
//  EVFImage.m
//  EVIME
//
//  Created by Evgeny on 16/03/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFImage.h"


@implementation EVFImage

@dynamic evimeUUID;
@dynamic imageFile;
@dynamic status;

@end
