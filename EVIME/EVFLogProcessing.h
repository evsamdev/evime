//
//  EVFLogProcessing.h
//  EVIME
//
//  Created by Evgeny on 17/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EVFLogNote.h"

@interface EVFLogProcessing : NSObject

-(void)uploadAsynchLogNote:(EVFLogNote *)logNote;
-(void)uploadSynchLogNote:(EVFLogNote *)logNote;

@end
