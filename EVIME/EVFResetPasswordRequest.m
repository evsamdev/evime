//
//  EVFResetPasswordRequest.m
//  EVIME
//
//  Created by Evgeny on 5/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFResetPasswordRequest.h"
#import "EVFAccount.h"
#import "EVFAppDelegate.h"
#import "SBJson.h"
#import "EVFCryptoHelper.h"

@implementation EVFResetPasswordRequest

-(void)sendRequestForAccount:(EVFAccount *)account {
    
    NSString *passKey = [EVFCryptoHelper resetRequestKeyForUsername:account.username andSalt:@"7236543"];
    NSString *resetKey = [EVFCryptoHelper passKeyForUsername:account.username andPassword:[[NSUUID UUID] UUIDString]];
    //NSLog(@"passKey:%@",passKey);
    //NSLog(@"resetKey:%@",resetKey);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/reset_password.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    [requestData setObject:account.username forKey:@"username"];
    [requestData setObject:passKey forKey:@"passKey"];
    [requestData setObject:resetKey forKey:@"resetKey"];
    
    NSString* str = [NSString stringWithFormat:@"data=%@",[requestData JSONRepresentation]];
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection failed!" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                   [alertView show];
                                   //NSLog(@"request error: %@", error.localizedDescription);
                                   [self.delegate resetPasswordDidFail];
                                   
                               }else{
                                   
                                   NSString* responseString = [[NSString alloc] initWithData:data
                                                                                    encoding:NSUTF8StringEncoding];
                                   //NSLog(@"responseString: %@", responseString);
                                   SBJsonParser *parser = [[SBJsonParser alloc] init];
                                   NSDictionary *responseDict = [parser objectWithString:responseString];
                                   //NSLog(@"responseDict: %@", responseDict);
                                   
                                   if(responseDict){
                                       
                                       if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:0]]){
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"An email has been sent to your account's email address." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate resetPasswordDidFinish];
                                           
                                           
                                       }else if([[responseDict objectForKey:@"status"] isEqual:[NSNumber numberWithInt:-4]]){
                                           
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User with that username does not exist." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate resetPasswordDidFail];
                                           
                                       }else{
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unexpected error!" message:@"Please try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                           [alertView show];
                                           
                                           [self.delegate resetPasswordDidFail];
                                       }
                                       
                                       
                                   }else{
                                       
                                       UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unexpected error!" message:@"Please try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                       [alertView show];
                                       
                                       [self.delegate resetPasswordDidFail];
                                       
                                   }
                                   
                                   
                               }
                               
                               
                           }];
}


@end
