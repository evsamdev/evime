//
//  EVFSettingsViewController.h
//  EVIME
//
//  Created by Evgeny on 17/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFSettingsViewController : UIViewController

- (IBAction)emailToSupport:(UIButton *)sender;


- (IBAction)goToSite:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;

- (IBAction)logout:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;


@end
