//
//  EVFProperty.m
//  EVIME
//
//  Created by Evgeny on 29/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFProperty.h"


@implementation EVFProperty

@dynamic key;
@dynamic value;
@dynamic status;

@end
