//
//  EVFReplayViewController.m
//  EVIME
//
//  Created by Evgeny on 26/10/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFReplayViewController.h"
#import "EVFElementView.h"
#import "EVFEvime.h"
#import "EVFImageDetailViewController.h"
#import "EVFEvimesListViewController.h"
#import "EVFAppDelegate.h"
#import "EVFSendViewController.h"
#import <MessageUI/MessageUI.h>
#import "EVFEmailGenerator.h"
#import "EVFLogNote.h"
#import "EVFLogProcessing.h"

@interface EVFReplayViewController ()<UIDocumentInteractionControllerDelegate,MFMailComposeViewControllerDelegate>
@property (nonatomic, strong) NSMutableArray *elementsList;
@property (nonatomic, strong) NSArray *elementsDataList;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@implementation EVFReplayViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
	self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    if(self.isFirstPage){
        self.navigationItem.hidesBackButton = YES;
    }
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    self.replayTableView.backgroundColor = [UIColor clearColor];
    
    self.elementsDataList = [self.pageData objectForKey:@"elements"];
    
    //NSLog(@"elementsDataList: %@", self.elementsDataList);
    
    self.elementsList = [[NSMutableArray alloc] init];
    
    for(NSDictionary *contentData in self.elementsDataList){
        
        ElementType type = [[contentData objectForKey:@"type"] integerValue];
        float height = [[contentData objectForKey:@"height"] floatValue];
        if(height==0){
            height = 50.0;
        }
        CGRect newElementFrame;
        UIView *contentView;
        switch (type) {
            case kElementTypeImage:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                
                NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *myDocsPath    = [myPathList  objectAtIndex:0];
                NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:self.currentEvime.uuid];
                NSString *imageFile = [currentImagesDir stringByAppendingPathComponent:[NSString stringWithFormat:@"th_%@",[contentData objectForKey:@"content"]]];
                UIImage *contentImage = [UIImage imageWithContentsOfFile:imageFile];
                float imageHeight = height-10;
                float imageX = 0;
                float imageWidth = 280;
                
                if(contentImage){
                
                    if(contentImage.size.height>contentImage.size.width){
                        
                        float aspectRatio = contentImage.size.height/contentImage.size.width;
                        
                        if(imageHeight>280){
                            imageHeight = 280;
                        }
                        
                        imageWidth = imageHeight/aspectRatio;
                        imageX =(280-imageWidth)/2.0;
                        
                    }else{
                        float aspectRatio = contentImage.size.width/contentImage.size.height;
                        
                        float maxImageHeight = 280/aspectRatio;
                        
                        if(imageHeight>maxImageHeight){
                            imageHeight = maxImageHeight;
                        }
                        
                        imageWidth = imageHeight*aspectRatio;
                        imageX= (280-imageWidth)/2.0;
                        
                    }
                    
                    UIImageView *imageContentView = [[UIImageView alloc] initWithFrame:CGRectMake(imageX, 10, imageWidth, imageHeight)];
                    
                    imageContentView.userInteractionEnabled = YES;
                    contentView = imageContentView;
                    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImage:)];
                    [contentView addGestureRecognizer:tapGR];
                    imageContentView.image =  contentImage;
                }
                break;
            }
                
            case kElementTypeText:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                UITextView *textContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, height-10)];
                //textContentView.backgroundColor = [UIColor colorWithWhite:0.7 alpha:1.0];
                textContentView.returnKeyType = UIReturnKeyDefault;
                textContentView.font=[UIFont systemFontOfSize:16];
                textContentView.editable = NO;
                textContentView.scrollEnabled = NO;
                contentView = textContentView;
                textContentView.backgroundColor = [UIColor clearColor];
                textContentView.text = [contentData objectForKey:@"content"];
                break;
            }
                
            case kElementTypeButton:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                UITextView *buttonContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, height-10)];
                buttonContentView.returnKeyType = UIReturnKeyDefault;
                buttonContentView.font=[UIFont boldSystemFontOfSize:16];
                buttonContentView.textAlignment = NSTextAlignmentCenter;
                buttonContentView.textColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:1.0];
                buttonContentView.editable = NO;
                buttonContentView.scrollEnabled = NO;
                buttonContentView.backgroundColor = [UIColor clearColor];
                contentView = buttonContentView;
                UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSubPage:)];
                [contentView addGestureRecognizer:tapGR];
                buttonContentView.text = [contentData objectForKey:@"content"];
                break;
            }
                
            case kElementTypeHeader:{
                newElementFrame = CGRectMake(20, 0, 280, height);
                UITextView *textContentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, 280, height-10)];
                textContentView.returnKeyType = UIReturnKeyDefault;
                textContentView.textAlignment = NSTextAlignmentCenter;
                textContentView.font=[UIFont boldSystemFontOfSize:16];
                textContentView.editable = NO;
                textContentView.scrollEnabled = NO;
                textContentView.backgroundColor = [UIColor clearColor];
                contentView = textContentView;
                textContentView.text = [contentData objectForKey:@"content"];
                break;
            }
                
            default:
                break;

        }
        EVFElementView  *elementNextView = [[EVFElementView alloc] initWithFrame:newElementFrame];
        elementNextView.type = type;
        [elementNextView addSubview:contentView];
        [self.elementsList addObject:elementNextView];
    
    }
    
    [self.replayTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(void)showImage:(UITapGestureRecognizer *)sender{
    //NSLog(@"showImage");
    
    
    EVFElementView  *elementView = (EVFElementView  *)[sender.view superview];
    NSDictionary *contentData = [self.elementsDataList objectAtIndex:[self.elementsList indexOfObject:elementView]];
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *myDocsPath    = [myPathList  objectAtIndex:0];
    NSString *currentImagesDir = [myDocsPath stringByAppendingPathComponent:self.currentEvime.uuid];
    NSString *imageFile = [currentImagesDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[contentData objectForKey:@"content"]]];
    
    UIDocumentInteractionController *documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:imageFile]];
    documentInteractionController.name = @"My Image";
    // Configure Document Interaction Controller
    [documentInteractionController setDelegate:self];
    
    // Preview PDF
    [documentInteractionController presentPreviewAnimated:YES];
    
    
    /*
    UIImage *contentImage = [UIImage imageWithContentsOfFile:imageFile];
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    
    EVFImageDetailViewController * imageDetailViewController = (EVFImageDetailViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFImageDetail"];
    
    
    
    imageDetailViewController.currentImage = contentImage;
    [self.navigationController pushViewController:imageDetailViewController animated:YES];
     */
}

#pragma mark -
#pragma mark Document Interaction Controller Delegate Methods
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}


-(void)showSubPage:(UITapGestureRecognizer *)sender{
    
    //NSLog(@"showSubPage");
    
    EVFElementView  *elementView = (EVFElementView  *)[sender.view superview];
    NSDictionary *contentData = [self.elementsDataList objectAtIndex:[self.elementsList indexOfObject:elementView]];
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    
    EVFReplayViewController * replayViewController = (EVFReplayViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFReplay"];
    replayViewController.currentEvime = self.currentEvime;
    replayViewController.pageData = [contentData objectForKey:@"page"];
    replayViewController.title = [contentData objectForKey:@"content"];
    [self.navigationController pushViewController:replayViewController animated:YES];
    
}


- (IBAction)editEvime:(UIBarButtonItem *)sender {
    
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setEvimeForEdit:self.currentEvime];
   
    NSArray *controllers = [self.navigationController childViewControllers];
    
    for(UIViewController *vc in controllers){
        if([vc isKindOfClass:[EVFEvimesListViewController class]]){
            
            [self.navigationController popToViewController:vc animated:NO];
        }
        
    }
   
}

- (IBAction)moveEvimeToTrash:(UIBarButtonItem *)sender {
    
    self.currentEvime.trash = [NSNumber numberWithInt:1];
    self.currentEvime.status = @"notuploaded";
    self.currentEvime.deleteDate = [NSDate date];
    NSError *error;
    [self.managedObjectContext save:&error];
    [self closeEvime:nil];
    
}

- (IBAction)closeEvime:(UIBarButtonItem *)sender {
    
    NSArray *controllers = [self.navigationController childViewControllers];
    
    for(UIViewController *vc in controllers){
        if([vc isKindOfClass:[EVFEvimesListViewController class]]){
            
            [self.navigationController popToViewController:vc animated:YES];
        }
        
    }
        
}

- (IBAction)sendEvime:(UIBarButtonItem *)sender {
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
    
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                      bundle:nil];
        
        EVFSendViewController * sendViewController = (EVFSendViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFSend"];
        
        sendViewController.evimeToSend = self.currentEvime;
        
        [self.navigationController pushViewController:sendViewController animated:YES];
    }else{
        
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            
            NSString *body = [[[EVFEmailGenerator alloc] init] htmlEmailBodyForEvime:self.currentEvime];
            [mailViewController setSubject:self.currentEvime.title];
            [mailViewController setMessageBody:body isHTML:YES];
            [self presentViewController:mailViewController animated:YES completion:nil];
            
        } else {
            //NSLog(@"Device is unable to send email in its current state.");
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Device is unable to send email in its current state." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }

    
    
    
    }

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    NSString *feedbackMsg;
    UIAlertView* alert;
    Boolean alertShow = YES;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            feedbackMsg = @"Mail sending canceled";
            alertShow = NO;
            break;
        case MFMailComposeResultSaved:
            feedbackMsg = @"Mail saved";
            alertShow = NO;
            break;
        case MFMailComposeResultSent:{
            feedbackMsg = @"Mail sent";
            alertShow = NO;
            EVFLogNote *logNote = [[EVFLogNote alloc] init];
            logNote.eventId = 101;
            logNote.note = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] deviceUUID];
            logNote.username = @"Anonimus";
            [[[EVFLogProcessing alloc] init] uploadAsynchLogNote:logNote];
        }
            break;
        case MFMailComposeResultFailed:
            feedbackMsg = @"Mail sending failed";
            break;
        default:
            feedbackMsg = @"Mail not sent";
            break;
    }
    if(alertShow){
        alert = [[UIAlertView alloc] initWithTitle:@"Mail" message:feedbackMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.elementsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EVFElementView *elementView = [self.elementsList objectAtIndex:indexPath.row];
    elementView.tag = 1001;
    static NSString *CellIdentifier = @"ElementCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    EVFElementView *elementViewInCell =  (EVFElementView *)[cell viewWithTag:1001];
    [elementViewInCell removeFromSuperview];    
    [cell addSubview:elementView];
    cell.tag = indexPath.row+100000;
    
    if(elementView.type==kElementTypeButton){
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EVFElementView *elementView = [self.elementsList objectAtIndex:indexPath.row];
    
    return elementView.frame.size.height;
}



@end
