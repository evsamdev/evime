//
//  EVFLoadingProcessing.h
//  EVIME
//
//  Created by Evgeny on 15/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EVFEvime;
@interface EVFLoadingProcessing : NSObject

-(void)downloadCurrentEvimes;
-(void)uploadCurrentChanges;
-(void)updateToken;
-(void)logout;
-(void)uploadImages;

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundLoadingTask;

@end
