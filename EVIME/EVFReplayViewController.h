//
//  EVFReplayViewController.h
//  EVIME
//
//  Created by Evgeny on 26/10/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EVFEvime;
@interface EVFReplayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *replayTableView;
@property (strong, nonatomic) NSMutableDictionary *pageData;
@property (strong, nonatomic) EVFEvime *currentEvime;

- (IBAction)editEvime:(UIBarButtonItem *)sender;
- (IBAction)moveEvimeToTrash:(UIBarButtonItem *)sender;
- (IBAction)closeEvime:(UIBarButtonItem *)sender;

- (IBAction)sendEvime:(UIBarButtonItem *)sender;


@property (nonatomic) BOOL isFirstPage;

@end
