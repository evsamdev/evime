//
//  EVFSendViewController.h
//  EVIME
//
//  Created by Evgeny on 17/11/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EVFEvime;
@interface EVFSendViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

- (IBAction)cancel:(UIBarButtonItem *)sender;

- (IBAction)send:(UIBarButtonItem *)sender;


- (IBAction)inviteUser:(UIButton *)sender;

@property (strong, nonatomic) EVFEvime *evimeToSend;

@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;

@property (weak, nonatomic) IBOutlet UITextField *evimeTitleTextField;

@property (weak, nonatomic) IBOutlet UILabel *myUsernameLabel;

@property (weak, nonatomic) IBOutlet UILabel *myFullNameLabel;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (weak, nonatomic) IBOutlet UILabel *fullNameTextField;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIButton *sendByEmailButton;

- (IBAction)sendByEmail:(UIButton *)sender;



@end
