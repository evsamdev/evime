//
//  EVFImageDetailViewController.h
//  EVIME
//
//  Created by Evgeny on 31/10/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFImageDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *fullscreenImageView;
@property (strong, nonatomic) UIImage *currentImage;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;

@end
