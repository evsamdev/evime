//
//  EVFLogProcessing.m
//  EVIME
//
//  Created by Evgeny on 17/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFLogProcessing.h"
#import "SBJson.h"

@implementation EVFLogProcessing


-(void)uploadAsynchLogNote:(EVFLogNote *)logNote{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/event_log.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];

    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];

    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    if(logNote.username)[requestData setObject:logNote.username forKey:@"username"];
    if(logNote.note)[requestData setObject:logNote.note forKey:@"note"] ;
    [requestData setObject:[NSNumber numberWithInteger:logNote.eventId] forKey:@"eventId"];
    //NSLog(@"requestData:%@",requestData);

    NSString *dataJsonString= [requestData JSONRepresentation];
    NSString *dataJsonStringEncoded = [[dataJsonString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];

    NSString* str = [NSString stringWithFormat:@"data=%@",dataJsonStringEncoded];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if(error){
                                   
                                   //NSLog(@"request error: %@", error.localizedDescription);
                                   
                               }else{
                                   
                                   //NSString* responseString = [[NSString alloc] initWithData:data
                                   //                                                 encoding:NSUTF8StringEncoding];
                                   //NSLog(@"responseString: %@, length: %lu", responseString,(unsigned long)data.length);
                               }
                           }];
    
}

-(void)uploadSynchLogNote:(EVFLogNote *)logNote{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://enotekit.com/mobile_app/api/event_log.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"678346293546726735642734691" forHTTPHeaderField:@"ClientKey"];
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    
    if(logNote.username)[requestData setObject:logNote.username forKey:@"username"];
    if(logNote.note)[requestData setObject:logNote.note forKey:@"note"] ;
    [requestData setObject:[NSNumber numberWithInteger:logNote.eventId] forKey:@"eventId"];
    //NSLog(@"requestData:%@",requestData);
    NSString *dataJsonString= [requestData JSONRepresentation];
    NSString *dataJsonStringEncoded = [[dataJsonString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString* str = [NSString stringWithFormat:@"data=%@",dataJsonStringEncoded];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    //NSLog(@"str:%@",str);
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error;
    NSData *data;
    NSURLResponse *response;
    data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if(error){
        
       // NSLog(@"request error: %@", error.localizedDescription);
        
    }else{
        
        //NSString* responseString = [[NSString alloc] initWithData:data
         //                                                encoding:NSUTF8StringEncoding];
        //NSLog(@"responseString: %@", responseString);
    }
    
}



@end
