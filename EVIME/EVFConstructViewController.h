//
//  EVFConstructViewController.h
//  EVIME
//
//  Created by Evgeny on 30.09.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EVFEvime.h"

@interface EVFConstructViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *constructTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *elementsSourceScrollVew;
@property (weak, nonatomic) IBOutlet UIButton *editModeButton;
@property (strong, nonatomic) NSMutableDictionary *pageData;
@property (strong, nonatomic) NSMutableDictionary *bodyData;
@property (strong, nonatomic) EVFEvime *currentEvime;
@property (nonatomic) NSInteger pageNumber;

- (IBAction)editMode:(UIButton *)sender;
- (IBAction)replay:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *replayButton;

- (IBAction)closeEvime:(UIButton *)sender;


@end
