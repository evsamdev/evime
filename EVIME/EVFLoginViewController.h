//
//  EVFLoginViewController.h
//  EVIME
//
//  Created by Evgeny on 1/12/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)login:(UIButton *)sender;

- (IBAction)forgotPassword:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end
