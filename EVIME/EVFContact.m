//
//  EVFContact.m
//  EVIME
//
//  Created by Evgeny on 29/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFContact.h"


@implementation EVFContact

@dynamic fullName;
@dynamic group;
@dynamic username;
@dynamic user;

@end
