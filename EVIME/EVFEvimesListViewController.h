//
//  EVFEvimesListViewController.h
//  EVIME
//
//  Created by Evgeny on 27/10/2013.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EVFEvimesListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, copy) NSString *box;

- (IBAction)createNewEvime:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet UITableView *evimesTableView;
- (IBAction)editMode:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

@property (nonatomic) BOOL isPushToCreateNewEvime;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteAllButton;
- (IBAction)deleteAll:(UIBarButtonItem *)sender;


@end
