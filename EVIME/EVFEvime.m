//
//  EVFEvime.m
//  EVIME
//
//  Created by Evgeny on 25/04/2014.
//  Copyright (c) 2014 Evforest. All rights reserved.
//

#import "EVFEvime.h"


@implementation EVFEvime

@dynamic body;
@dynamic box;
@dynamic createDate;
@dynamic deleteDate;
@dynamic group;
@dynamic index;
@dynamic lastEditDate;
@dynamic readyness;
@dynamic receiveDate;
@dynamic sendDate;
@dynamic status;
@dynamic title;
@dynamic trash;
@dynamic user;
@dynamic userFrom;
@dynamic userFullFrom;
@dynamic userFullTo;
@dynamic userTo;
@dynamic uuid;
@dynamic sourceUUID;

@end
