//
//  TGACryptoHelper.h
//  CheckPoint
//
//  Created by Evgeny on 18.08.13.
//  Copyright (c) 2013 vittoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EVFCryptoHelper : NSObject
+(NSString*)passKeyForUsername:(NSString *)username andPassword:(NSString *)password;
+(void)saveSessionKey:(NSString *)sessionKey;
+(NSString *)loadSessionKey;
+(void)deleteSessionKey;
+(NSString*)resetRequestKeyForUsername:(NSString *)username andSalt:(NSString *)salt;
@end
