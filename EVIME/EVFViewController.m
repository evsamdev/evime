//
//  EVFViewController.m
//  EVIME
//
//  Created by Evgeny on 28.09.13.
//  Copyright (c) 2013 Evforest. All rights reserved.
//

#import "EVFViewController.h"
#import "EVFConstructViewController.h"
#import "EVFEvime.h"
#import "EVFAppDelegate.h"
#import "EVFEvimesListViewController.h"
#import "SBJson.h"
#import "EVFProperty.h"
#import "EVFLoginViewController.h"
#import "EVFCryptoHelper.h"
#import "EVFLoadingProcessing.h"

@interface EVFViewController ()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *boxesList;
@property (strong, nonatomic) NSArray *evimesCountList;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
- (IBAction)login:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginButton;

@end

@implementation EVFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //NSLog(@"viewDidLoad");
	// Do any additional setup after loading the view, typically from a nib.
    self.managedObjectContext = [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [NSTimer scheduledTimerWithTimeInterval:180.0 target:self selector:@selector(updateDatabase) userInfo:nil repeats:YES];
    
    self.refreshControl = [[UIRefreshControl alloc]
                           init];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(updateDatabase) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFProperty"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"key" ascending:NO],nil];
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"key=='IsAuthenticated'"];
    [fetchRequest setPredicate:predicate];
    EVFProperty *isAuthenticatedProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
    
    predicate = [NSPredicate predicateWithFormat:@"key=='Username'"];
    [fetchRequest setPredicate:predicate];
    EVFProperty *usernameProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
    
    predicate = [NSPredicate predicateWithFormat:@"key=='FullName'"];
    [fetchRequest setPredicate:predicate];
    EVFProperty *fullNameProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
    
    NSString *sessionKey = [EVFCryptoHelper loadSessionKey];
    
    NSString *deviceUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
#if TARGET_IPHONE_SIMULATOR
    deviceUUID = @"000000-0000-0000-0000-0000";
#endif
    
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setDeviceUUID:deviceUUID];
    
    self.boxesList = [[NSArray alloc] initWithObjects:
                      [NSDictionary  dictionaryWithObjectsAndKeys:@"Drafts",@"title",@"draft",@"box",nil],
                      [NSDictionary  dictionaryWithObjectsAndKeys:@"Inbox",@"title",@"inbox",@"box",nil],
                      [NSDictionary  dictionaryWithObjectsAndKeys:@"Sent",@"title",@"sent",@"box",nil],
                      [NSDictionary  dictionaryWithObjectsAndKeys:@"Trash",@"title",@"trash",@"box",nil],
                      nil];
    
    if(isAuthenticatedProperty && sessionKey && usernameProperty){
        
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsAuthenticated:YES];
        
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setSessionKey:sessionKey];
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setUsername:usernameProperty.value];
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setUserFullName:fullNameProperty.value];
        
        [[[EVFLoadingProcessing alloc] init] downloadCurrentEvimes];
        
        [self updateIconBadge];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateIconBadge) name:@"com.evforest.StateOfEvimesListChanged" object:nil];
}

-(void)applicationDidBecomeActive{
    //NSLog(@"applicationDidBecomeActive");
    [self updateDatabase];

}


-(void)updateDatabase{
    //NSLog(@"updateDatabase");
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        
        [[[EVFLoadingProcessing alloc] init] downloadCurrentEvimes];
        [[[EVFLoadingProcessing alloc] init] uploadCurrentChanges];
        [self performSelector:@selector(endRefreshing) withObject:nil afterDelay:2.0];
          
    }else{
        
        if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] sessionKey]){
    
            [self.navigationController popToRootViewControllerAnimated:NO];
            [self logout];
        }
        
        [self.refreshControl endRefreshing];
    }
    
    

}

-(void)updateIconBadge{
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFEvime"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"box == 'inbox' && trash==0 && user == %@ && readyness = 1",[(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] username]];
        [fetchRequest setPredicate:predicate];
        NSArray *evimesUnreadList=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        if(evimesUnreadList.count>0){
            [UIApplication sharedApplication].applicationIconBadgeNumber = evimesUnreadList.count;
        }
    }

}


-(void)endRefreshing{

    [self.refreshControl endRefreshing];

}


-(void)viewWillAppear:(BOOL)animated{
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isLogoutAction]){
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsLogoutAction:NO];
        [self logout];
    }
    [self.tableView reloadData];
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated]){
       
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor clearColor];
        self.navigationItem.leftBarButtonItem.enabled = NO;
    }else{
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
        self.navigationItem.leftBarButtonItem.enabled = YES;
    }
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isLoginAction]){
        [self login:nil];
    }
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.boxesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *boxName = [[self.boxesList objectAtIndex:indexPath.row] objectForKey:@"title"];
    static NSString *CellIdentifier = @"BoxCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.textLabel.text = boxName;
    
    NSString *box = [[self.boxesList objectAtIndex:indexPath.row] objectForKey:@"box"];
    if(![(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated] && ([box isEqualToString:@"inbox"] || [box isEqualToString:@"sent"])){
        
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }else{
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self deselectCellAtIndexPath:indexPath];
    NSString *box = [[self.boxesList objectAtIndex:indexPath.row] objectForKey:@"box"];
    if(![(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isAuthenticated] && ([box isEqualToString:@"inbox"] || [box isEqualToString:@"sent"])){
        return;
    }
    
    NSDictionary *boxData = [self.boxesList objectAtIndex:indexPath.row];
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EVFEvimesListViewController * evimesListViewController = (EVFEvimesListViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFEvimesList"];
    evimesListViewController.box = [boxData objectForKey:@"box"];
    evimesListViewController.title = [boxData objectForKey:@"title"];
    [self.navigationController pushViewController:evimesListViewController animated:YES];
    
    

}

-(void)deselectCellAtIndexPath:(NSIndexPath *)indexPath{

    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];


}

#pragma mark - Actions

- (IBAction)createNewEvime:(UIBarButtonItem *)sender {

    NSDictionary *boxData = [self.boxesList objectAtIndex:0];
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EVFEvimesListViewController * evimesListViewController = (EVFEvimesListViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFEvimesList"];
    evimesListViewController.box = [boxData objectForKey:@"box"];
    evimesListViewController.title = [boxData objectForKey:@"title"];
    evimesListViewController.isPushToCreateNewEvime = YES;
    [self.navigationController pushViewController:evimesListViewController animated:YES];
    
}

- (void)logout {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EVFProperty"];
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"key" ascending:NO],nil];
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"key=='IsAuthenticated'"];
    [fetchRequest setPredicate:predicate];
    EVFProperty *isAuthenticatedProperty = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error].lastObject;
    if(isAuthenticatedProperty){
        [self.managedObjectContext deleteObject:isAuthenticatedProperty];
        [self.managedObjectContext save:&error];
    }
    [EVFCryptoHelper deleteSessionKey];
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsAuthenticated:NO];
    [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setSessionKey:nil];
   
    [self.tableView reloadData];
    [[[EVFLoadingProcessing alloc] init] logout];
    
}

- (IBAction)login:(UIBarButtonItem *)sender {
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EVFLoginViewController * loginViewController = (EVFLoginViewController *)[sb instantiateViewControllerWithIdentifier:@"EVFLogin"];
    
    if([(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] isLoginAction]){
        [(EVFAppDelegate *)[[UIApplication sharedApplication] delegate] setIsLoginAction:NO];
         [self.navigationController pushViewController:loginViewController animated:NO];
    }else{
         [self.navigationController pushViewController:loginViewController animated:YES];
    }
}
@end
