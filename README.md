# README #

Mobile Application EVIME
iOS v7.0+

New concept of mobile communication. This app allows its users to create complex, multi-page notes including headings, text and photos. The best part is, you can send these notes instantly to others either through the Enotekit app or via email. In comparison to plain text emails, you can build blocks of content that you create and send.
Notes consist of little blocks Edit, move or delete each block of a note separately Notes can contain active links to other page Active links allow to divide your note in sections and build a hierarchic structure of the note pages Add photos from your photo library to any part of the note View each photo separately with zoom and save ability Easily store your notes as drafts just for yourself
You can send your note privately to another Enotekit user. If the recipient is not an Enotekit user, you can still send your note via traditional email. The email content will be similar to your note.